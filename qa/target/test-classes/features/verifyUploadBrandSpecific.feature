Feature: brand specific check for the upload

  Scenario Outline: simple upload
    Given I am on the home page
    When I open the upload tab
    And I upload "<filePath>"
    Then My file is visible in the list
    And I can add brands "<brands>"
    And I can fill in metadata and confirm upload
    And Uploaded image is visible on search page for brands "<matchingBrandFilters>"
    And Uploaded image is not visible on search page for brands "<nonMatchingBrandFilters>"
    Examples:
      | filePath          | brands                       | matchingBrandFilters | nonMatchingBrandFilters                                                                      |
      | images/icon2.jpeg | net.persgroep.brand.demorgen | De Morgen            | Humo, AD, Brabants Dagblad, BN DeStem, De Gelderlander, De Stentor, Het Laatste Nieuws, Nina |