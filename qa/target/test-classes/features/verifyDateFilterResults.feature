
Feature: verify results when specifying a date filter

  Scenario Outline: Verify the shooting and import date filters
    Given I am on the home page
    When I select a shooting date period of "<selectedShootingDate>"
    And I select a import date period of "<selectedImportDate>"
    And I press search
    Then I expect to get "results"
    And I expect to see pictures with shooting date equals "<selectedShootingDate>"
    And I expect to see pictures with importDate equals "<selectedImportDate>"

    Examples:
      | selectedImportDate | selectedShootingDate |
      | Today & Yesterday  | Today & Yesterday    |
      | Past 30 days       | Past 30 days         |
      | Yesterday          | All                  |