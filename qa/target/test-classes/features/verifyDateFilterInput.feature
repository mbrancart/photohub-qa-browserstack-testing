@run
Feature: verifying correctness of date filter inputs

  Scenario Outline: Verify the shooting and import date inputs
    Given I am on the home page
    When I search for "<searchIncluded>" and "<searchExcluded>"
    And I select an import date range from "<importDateFrom>" to "<importDateTo>" in the text input
    And I press search
    Then I expect to get "results"
    And The "import" date range filter is still correct
    And The "Import date" of the picture is between "<importDateFrom>" and "<importDateTo>"
    Examples:
      | searchIncluded | searchExcluded | importDateFrom | importDateTo |
      | george*        | be             | 01/04/2019 | 10/04/2019 |

  Scenario: Verify shooting date removal
    Given I am on the home page
    When I select a shooting date period of "Past 30 days"
    And I press search
    And I remove the shooting date using the cross
    And I press search
    Then I expect the "shootingTo" parameter to be empty