
  Feature: verify full view of Images

    Scenario: Verify metadata of a random image
      Given I am on the home page
      When I search for "Trump"
      And I press search
      And I open the full view of an image
      Then Information of the image is displayed