
Feature: Verify selection of images

  Scenario: verify selection then unselection
    Given I am on the home page
    When I "select" a random image
    And I "unselect" a random image
    Then The image is not selected anymore


  Scenario: verify that I can select multiple images without search
    Given I am on the home page
    When I select "2" images by using "short-key on toggle"
    Then The images are selected
    And I can see my selection on the bottom
    And I can delete an image from the selection


  Scenario Outline: verify that I can select multiple images after a search
    Given I am on the home page
    When I search for "<searchIncluded>" and "<searchExcluded>"
    And I press search
    Then I select "<numberOfImages>" images by using "<action>"
    And The images are selected
    And I can see my selection on the bottom
    Examples:
      | searchIncluded | searchExcluded | numberOfImages | action          |
      | Obama*         |                | 2              | short-key       |
      | Obama*         | Barack*        | 1              | click on toggle |

  Scenario Outline: verify that when I select images and i scroll down the images remain the same
    Given I am on the home page
    When I press search
    And I select "<numberOfImages>" images by using "<action>"
    And I scroll down the result page
    Then I can see my selection on the bottom and the "<numberOfImages>" remain the same
    Examples:
      | numberOfImages | action          |
      | 3              | short-key       |
      | 1              | click on toggle |


  Scenario Outline: Verify image selections in full view
    Given I am on the home page
    And I press search
    And I open the full view of an image
    When I select the next "<numberOfImages>" images
    And I close the full view
    Then I can see my selection of "<numberOfImages>" images on the bottom
    Examples:
      | numberOfImages |
      | 1              |
      | 2              |
      | 10             |

  Scenario: Verify that the banner is visible when preview is opened
    Given I am on the home page
    When I "select" a random image
    And I open the full view of an image
    Then I can see my selection on the bottom


  Scenario: Verify that banner appears when selecting image in full view
    Given I am on the home page
    And I open the full view of an image
    And I can see my selection on the bottom
    When I select the next "2" images
    Then I can see my selection of "2" images on the bottom

    Scenario: Verify that banner disappears after deleting an image in the full view
      Given I am on the home page for brand "net.persgroep.brand.ad"
      And I open the full view of an image
      When I add the current image to selection
      Then I can see my selection of "1" images on the bottom
      And I remove the current image from selection
      Then I can see my selection of "0" images on the bottom


  Scenario: Verify that banner disappears after deleting an image on the banner
    Given I am on the home page for brand "net.persgroep.brand.ad"
    And I open the full view of an image
    When I select the next "2" images
    Then I can see my selection of "2" images on the bottom
    And I remove the first image from selection
    Then I can see my selection of "1" images on the bottom



