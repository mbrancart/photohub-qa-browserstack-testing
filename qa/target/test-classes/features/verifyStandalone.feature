
  Feature: Verify standalone functionality

    Scenario: Verify that add to article button is not visible in standalone
      Given I am on the standalone home page
      When I select "3" images by using "click on toggle"
      Then the add images to article button is not visible
