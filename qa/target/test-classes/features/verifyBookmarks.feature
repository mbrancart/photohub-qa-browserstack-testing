
Feature: verify bookmarks

  Scenario: verify I can see pictures in the folders
    Given I am on the home page
    When I open the bookmarks tab
    And I open the folder named "AD/.ADN Voorselectie"
    Then I expect to get "results"

  Scenario Outline: verify I can see subfolder when clicking on the arrow
    Given I am on the home page
    When I open the bookmarks tab
    And I click on the arrow next to folder "<folderName>"
    Then The subfolders of "<folderName>" are visible
    Examples:
      | folderName |
      | AD         |


  Scenario: Verify next page is available
  Given I am on the home page
  When I open the bookmarks tab
  And I open the folder named "Primo/.Picture"
  Then I expect to get "results"
  And I press the next bookmarks page button
  Then I expect to get the next page of bookmarks

    #remove this test for now: will be available in the JS version
#  Scenario Outline: Verify sorting of results
#    Given I am on the home page
#    When I open the bookmarks tab
#    And I open the bookmark named "Folder 1"
#    And I order results by "<sorting>"
#    Then Results are ordered by "<sorting>"
#    Examples:
#      | sorting                     |
#      | Oldest (import date)        |
#      | Most recent (shooting date) |
#      | Oldest (shooting date)      |

#  Scenario: change the view to small
#    Given I am on the home page
#    When I open the bookmarks tab
#    And I open the bookmark named "DM"
#    Then I expect to get "results"
#    And I change the view option to small images
#    Then I expect to get the result displayed with small images


