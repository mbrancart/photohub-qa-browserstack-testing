
Feature: Verify navigation between tabs

  Scenario Outline: Verify brand context after aborting upload
    Given I am on the home page for brand "<brand>" and redirect "<redirect>"
    When I open the upload tab
    And I upload "images/icon.png"
    And I open the search tab
    Then The brand is still "<brand>"
    And The redirect is still "<redirect>"
    Examples:
      | brand                               | redirect |
      | net.persgroep.brand.brabantsdagblad | anything |


  Scenario Outline: Verify navigation from search to upload and back
    Given I am on the home page for brand "<brand>" and redirect "<redirect>"
    When I open the upload tab
    Then The brand is still "<brand>"
    And The redirect is still "<redirect>"
    And I open the search tab
    Then The brand is still "<brand>"
    And The redirect is still "<redirect>"
    Examples:
      | brand                               | redirect |
      | net.persgroep.brand.brabantsdagblad | anything |


  Scenario Outline: Verify navigation from search to bookmarks and back
    Given I am on the home page for brand "<brand>" and redirect "<redirect>"
    When I open the bookmarks tab
    Then The brand is still "<brand>"
    And The redirect is still "<redirect>"
    And I open the search tab
    Then The brand is still "<brand>"
    And The redirect is still "<redirect>"
    Examples:
      | brand                               | redirect |
      | net.persgroep.brand.brabantsdagblad | anything |