
Feature: verify navigation of images

  Scenario Outline: Verify forward navigation of images
    Given I am on the home page
    When I open the full view of an image "<currentImageNr>"
    And  I press the next button
    Then the image "<nextImageNr>" is shown

    Examples:
      | currentImageNr | nextImageNr |
      | 0              | 1           |
      | 58             | 59          |


  Scenario Outline: Verify backward navigation of images
    Given I am on the home page
    When I open the full view of an image "<currentImageNr>"
    And  I press the previous button
    Then the image "<previousImageNr>" is shown

    Examples:
      | currentImageNr | previousImageNr |
      | 1              | 0               |
      | 59             | 58              |
  
  Scenario: Verify navigation to next page in full view
    Given I am on the home page
    When I open the full view of an image "50"
    And  I select the next "15" images
    Then the image "60" is shown

  Scenario: Verify navigation to previous page in full view
    Given I am on the home page
    And I press search
    When I scroll down the result page
    And I open the full view of an image "60"
    And  I press the previous button
    Then the image "59" is shown

  Scenario: Verify that next page is displayed on result page
    Given I am on the home page
    When I open the full view of an image "55"
    And  I select the next "5" images
    And I close the full view
    Then I expect to get the next page of images

  Scenario: Verify that loading is not appearing while reaching the last result
    Given I am on the home page
    When I search for "104419263"
    And I press search
    And I open the full view of an image "0"
    And I close the full view
    Then The loading is not visible on the last page