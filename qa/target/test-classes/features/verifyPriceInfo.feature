
Feature: verifyPriceInfo

  Scenario Outline: Price displayed is same as on diocontent webservice and on fullview
    Given I am on the home page
    When I search for "<Included>"
    And I press search
    Then I expect to get "result"
    And The price matches the price of diocontent
    Examples:
      | Included     |
      | testbeeld    |