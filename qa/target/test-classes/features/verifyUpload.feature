Feature: Verify upload functionality

  Scenario Outline: upload 2 images at the same time
    Given I am on the home page
    When I open the upload tab
    And I upload "<firstFilePath>" and "<secondFilePath>"
    Then My file is visible in the list
    And I can add brands "net.persgroep.brand.demorgen"
    And I can fill in metadata and confirm upload
    And Uploaded image is visible on search page
    Examples:
      | firstFilePath   | secondFilePath   |
      | images/icon.png | images/logo1.jpg |

  Scenario Outline: upload 2 images after each other in brand aware mode
    Given I am on the home page for brand "<brand>"
    When I open the upload tab
    And I upload "<firstFilePath>"
    Then My file is visible in the list
    And I upload additional files "<secondFilePath>"
    Then I select all images
    And I can fill in metadata and confirm upload
    And Uploaded image is visible on search page
    And The brand is still "<brand>"
    Examples:
      | firstFilePath   | secondFilePath   | brand                        |
      | images/icon.png | images/logo1.jpg | net.persgroep.brand.demorgen |

  Scenario Outline: verify error on caption and credits
    Given I am on the home page
    When I open the upload tab
    And I upload "<filePath>"
    Then My file is visible in the list
    And Error is displayed when caption and credits are empty
    Examples:
      | filePath        |
      | images/icon.png |

