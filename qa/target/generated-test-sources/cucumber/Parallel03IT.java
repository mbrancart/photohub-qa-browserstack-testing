
import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(
retryCount = 3, // Number of times retry should happen in case of failure
outputFolder = "target")
@CucumberOptions(
strict = false,
features = {"/Users/mbrancart/photohub-qa-browserstack-testing/qa/src/test/resources/features/verifyBrandFilter.feature"},
monochrome = false,
glue = "steps")
public class Parallel03IT {
}