package screens;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class SearchScreen extends BaseScreen {

    private WebDriver driver;
    private Select importDateDropdown, shootingDateDropdown, brandFilterDropdown;
    private String selectedImportDateFrom = "";
    private String selectedImportDateTo = "";
    private String selectedShootingDateFrom = "";
    private String selectedShootingDateTo = "";

    @FindBy(id = "including")
    public WebElement includingField;

    @FindBy(id = "excluding")
    public WebElement excludingField;

    @FindBy(id = "createTimespan")
    public WebElement importPeriodDropdown;

    @FindBy(className = "form-input__date-container")
    public WebElement selectedImportDate;

    // @FindBy(className = "form-input--shooting-date") doesn't work because there are 2 elemements matching the classname
    @FindBy(xpath = "//input[contains(@class,'form-input--shooting-date') and not (contains(@type,'hidden'))]")
    public WebElement selectedShootingDate;

    @FindBy(xpath = "//*[@id='shootingTimespan']/following-sibling::img[@class='form-input__date-container__close']")
    public WebElement removeShootingDateButton;

    @FindBy(id = "createTimespanFrom")
    public WebElement createTimestampFrom;

    @FindBy(id = "createTimespanTo")
    public WebElement createTimestampTo;

    @FindBy(id = "shootingTimespanFrom")
    public WebElement shootingTimestampFrom;

    @FindBy(id = "shootingTimespanTo")
    public WebElement shootingTimestampTo;

    @FindBy(id = "selectedBrand")
    public WebElement brandFilter;

    @FindBy(id = "shootingTimespan")
    public WebElement shootingPeriodDropdown;

    @FindBy(className = "button--primary")
    public WebElement searchButton;

    @FindBy(xpath = "//label[contains(@for,'dropdown__toggle-view')]")
    public WebElement imageViewSizeDropdown;

    @FindBy(xpath = "//a[contains(@data-value,'search-results__grid--small')]")
    public WebElement smallViewlink;

    @FindBy(xpath = "//a[contains(@data-value,'search-results__grid--large')]")
    public WebElement largeViewlink;

    @FindBy(className = "cur-month")
    public WebElement selectedCalendarMonth;

    @FindBy(className = "cur-year")
    public WebElement selectedCalendarYear;

    @FindBy(className = "flatpickr-prev-month")
    public WebElement previousMonthArrow;

    @FindBy(className = "flatpickr-next-month")
    public WebElement nextMonthArrow;

    public SearchScreen(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void clickSearch() {
        click(searchButton);
    }

    public void typeInclude(String text) {
        enterText(includingField, text);
    }

    public void typeExclude(String text) {
        enterText(excludingField, text);
    }

    public void selectImportDate(String period) {
        if (!period.contains("All")) {
            click(selectedImportDate);
            driver.findElement(By.xpath(String.format("//*[@class='form-input--date__wrapper__buttons']//*[text()='%s']", period))).click();
            selectedImportDateFrom = createTimestampFrom.getAttribute("value");
            selectedImportDateTo = createTimestampTo.getAttribute("value");
            click(selectedImportDate);
        }
    }

    public void selectImportPeriodInputText(String startDate, String endDate) {
        click(selectedImportDate);
        mandatoryPreClear();
        enterText(createTimestampTo, endDate);
        enterText(createTimestampFrom, startDate);
        selectedImportDateFrom = startDate;
        selectedImportDateTo = endDate;
        click(selectedImportDate);
    }

    private void mandatoryPreClear() {
        createTimestampFrom.clear();
        createTimestampTo.clear();
    }

    public void selectImportPeriodCalendar(String startDate, String endDate) {
        click(selectedImportDate);
        selectDayInCalendar(endDate);
        selectDayInCalendar(startDate);
        selectedImportDateFrom = startDate;
        selectedImportDateTo = endDate;
        click(selectedImportDate);
    }

    private void selectDayInCalendar(String date) {
        int refNumber = 0;
        boolean negative = false;
        try {
            enterText(selectedCalendarYear, String.valueOf(getYearFromDate(date)));
            refNumber = getMonthFromDate(date) - getSelectedMonthInCalendar();
            if (refNumber <= 0) {
                negative = true;
                refNumber = Math.abs(refNumber);
            }
            for (int i = 0; i < refNumber; i++) {
                if (negative)
                    click(previousMonthArrow);
                else
                    click(nextMonthArrow);
            }
            click(driver.findElement(By.xpath(String.format("//*[@class='dayContainer']/span[text()='%s' and not (contains(@class,'nextMonthDay'))]", getDayFromDate(date)))));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void selectShootingDate(String period) {
        if (!period.contains("All")) {
            click(selectedShootingDate);
            //no id for the div so need to find all elements
            driver.findElements(By.xpath(String.format("//*[@class='form-input--date__wrapper__buttons']//*[text()='%s']", period))).get(1).click();
            selectedShootingDateFrom = shootingTimestampFrom.getAttribute("value");
            selectedShootingDateTo = shootingTimestampTo.getAttribute("value");
            click(selectedShootingDate);
        }
    }

    public void selectBrand(String brand) {
        waitForElementToBeVisible(brandFilter, 5);
        initiateBrandFilterDropdown();
        brandFilterDropdown.selectByVisibleText(brand);
    }

    public String getSelectedBrand() {
        initiateBrandFilterDropdown();
        return brandFilterDropdown.getFirstSelectedOption().getText();
    }
    public List<String> getAllBrandOptions(){
        initiateBrandFilterDropdown();
        return getActualDropdownValues();
    }

    private List<String> getActualDropdownValues() {
        return brandFilterDropdown.getOptions()
                .stream()
                .map(dropdownElement -> dropdownElement.getText())
                .collect(Collectors.toList());
    }

    private void initiateBrandFilterDropdown() {
        if (brandFilterDropdown == null)
            brandFilterDropdown = new Select(brandFilter);
    }

    public boolean isBrandDropdownContainingBrands(List<String> listOfAvailableBrands) {
        return getAllBrandOptions().containsAll(listOfAvailableBrands);
    }

    public String getSelectedImportDate() {
        return importDateDropdown.getFirstSelectedOption().getText();
    }

    public String getSelectedShootingDate() {
        return shootingDateDropdown.getFirstSelectedOption().getText();
    }

    public void removeShootingDate(){ click(removeShootingDateButton,5); }

    public LocalDate getEndDateOfPeriod(String desiredValue) {
        String selectedValue = getSelectedDropDownValue(desiredValue);
        System.out.println("selected period filter value: " + selectedValue);
        LocalDateTime localDateTime = LocalDateTime.now();

        if (selectedValue.equals("YESTERDAY"))
            localDateTime = localDateTime.minusDays(1);

        return localDateTime.toLocalDate();
    }

    public LocalDate getStartDateOfPeriod(String desiredValue) {

        String selectedValue = getSelectedDropDownValue(desiredValue);
        LocalDateTime localDateTime = LocalDateTime.now();

        switch (selectedValue) {
            case "ALL":
                localDateTime = LocalDateTime.MIN;
                break;
            case "YESTERDAY":
            case "TODAY_AND_YESTERDAY":
                localDateTime = localDateTime.minusDays(1);
                break;
            case "PAST_THIRTY_DAYS":
                localDateTime = localDateTime.minusMonths(1);
                break;
        }
        return localDateTime.toLocalDate();

    }

    //returns selected value of dropdown (shooting or import date)
    private String getSelectedDropDownValue(String dropdown) {
        if (dropdown.contains("shooting"))
            return getAttribute(shootingDateDropdown.getFirstSelectedOption(), "value");
        else
            return getAttribute(importDateDropdown.getFirstSelectedOption(), "value");
    }

    public boolean checkImageViewSizeLargeIsSelected() {
        return imageViewSizeDropdown.getText().equalsIgnoreCase("Large");
    }

    public void setImageViewSizeToSmall() {
        click(imageViewSizeDropdown);
        click(smallViewlink);
    }

    public String getExpectedImportDateRange() {
        if (!StringUtils.isBlank(selectedImportDateFrom)||StringUtils.isBlank(selectedImportDateTo)) {
            System.out.println(String.format("expected date range is = %s - %s", selectedImportDateFrom, selectedImportDateTo));
            return String.format("%s - %s", selectedImportDateFrom, selectedImportDateTo);
        }
        else
            return "";
    }

    public String getExpectedShootingDateRange() {
        if (!StringUtils.isBlank(selectedShootingDateFrom)||StringUtils.isBlank(selectedShootingDateTo)) {
            System.out.println(String.format("expected date range is = %s - %s", selectedShootingDateFrom, selectedShootingDateTo));
            return String.format("%s - %s", selectedShootingDateFrom, selectedShootingDateTo);
        }
        else
            return "";
    }

    public String getActualImportDateRange() {
        System.out.println(String.format("actual date range is = %s", driver.findElement(By.className("form-input__date-container")).findElement(By.className("input")).getAttribute("value")));
        return driver.findElement(By.className("form-input__date-container")).findElement(By.className("input")).getAttribute("value");
    }

    public String getActualShootingDateRange() {
        System.out.println(String.format("actual date range is = %s", driver.findElements(By.className("form-input__date-container")).get(1).findElement(By.className("input")).getAttribute("value")));
        return driver.findElements(By.className("form-input__date-container")).get(1).findElement(By.className("input")).getAttribute("value");
    }

    public int getSelectedMonthInCalendar() throws ParseException {
        Date date;
        date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(selectedCalendarMonth.getText());
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH) + 1;
    }

    public String getParameter(String parameterName) throws MalformedURLException {
        return this.getQueryParameters().stream()
                .map(parameter -> parameter.split("="))
                .filter(stringArray -> stringArray.length > 1)
                .filter(stringArray -> stringArray[0].contains(parameterName))
                .findFirst()
                .map(stringArray -> stringArray[1])
                .orElse("");
    }

    //Date utils
    private int getYearFromDate(String date) {
        return Integer.parseInt(date.split("/", 3)[2]);
    }

    private int getMonthFromDate(String date) {
        return Integer.parseInt(date.split("/", 3)[1]);
    }

    private int getDayFromDate(String date) {
        return Integer.parseInt(date.split("/", 3)[0]);
    }

}
