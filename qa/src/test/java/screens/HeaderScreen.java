package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.openqa.selenium.By.tagName;

public class HeaderScreen extends BaseScreen {

    public HeaderScreen(WebDriver driver) {
        super(driver);
    }

    public String getJavaScriptUrl() {
        return driver.findElement(By.tagName("script")).getAttribute("src");
    }

    public String getCssUrl() {
        return driver.findElement(By.xpath("//link[@rel='stylesheet']")).getAttribute("href");
    }

    public String getJavaScriptContent() {
        driver.get(getJavaScriptUrl());
        return driver.findElement(tagName("pre")).getText();
    }

    public String getCssContent() {
        driver.get(getCssUrl());
        return driver.findElement(tagName("pre")).getText();
    }
}
