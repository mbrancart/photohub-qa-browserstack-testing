package screens;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.MetadataUtils;
import webdriver.WebDriverManagerUtil;

import java.util.List;

public class ResultScreen extends BaseScreen {

    WebDriver driver= WebDriverManagerUtil.getDriver();
    Actions actions=new Actions(driver);
    WebDriverWait wait = new WebDriverWait(driver,10);

    @FindBy(xpath = "/html/body/center/h1")
    public WebElement header;

    @FindBy(tagName = "body")
    public WebElement body;

    @FindBy(className = "search-results__grid")
    public WebElement resultGrid;

    @FindBy(className = "search-result")
    public List<WebElement> results;

    @FindBy(xpath = "//article[contains(@class,'search-result')] [1]")
    public WebElement firstElement;

    @FindBy(xpath = "//label[contains(@for, 'dropdown__toggle-sort')]")
    public WebElement sortingFilter;

    @FindBy(className = "banner")
    public WebElement selectionBanner;

    @FindBy(className = "banner__strip")
    public WebElement selectionBannerImages;

    @FindBy(className = "fjs_multiple_download_action__submit")
    public WebElement downloadImagesButton;

    @FindBy(className = "pagination__loader")
    public WebElement scrollElement;

    @FindBy(className = "search-results__query")
    public WebElement resultsFound;

    public ResultScreen(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public String getTitle(){
        return header.getText();
    }

    public String getBodyText(){
        return readText(body);
    }

    public List<WebElement> getResultList() {return results;}

    public int getNumberOfResults(){ return Integer.parseInt(resultsFound.getText().replaceAll(",","").replaceAll("\\+","").split(" ")[0]); }

    public void waitForList(String url){
        WebDriverWait homePageWait=new WebDriverWait(driver,15);
        try{
            homePageWait.until(ExpectedConditions.visibilityOf(firstElement));
        }catch (Exception ex){
            System.out.println("catched exception :"+ ex.getMessage());
            takeScreenshot();
            driver.get(url);
            homePageWait.until(ExpectedConditions.visibilityOf(firstElement));
        }
    }

    public boolean checkElementHasYellowBorder(WebElement element) {
        return element.getCssValue("outline").equalsIgnoreCase("rgb(255, 194, 15) solid 3px");
    }

    public void scrollAfterSearch(){
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
        //In case we use the scoll at the end of the results
        if(results.size()!=getNumberOfResults()){
            waitForScrollElementToBecomeHidden();
        }
        wait.until(ExpectedConditions.visibilityOf(driver.findElements(By.className("search-result")).get(results.size()-1)));
    }

    public void scroll(){
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
        waitForScrollElementToBecomeHidden();
        wait.until(ExpectedConditions.visibilityOf(driver.findElements(By.className("search-result")).get(results.size()-1)));
    }

    public void waitForScrollElementToBecomeVisible() {
        wait.until(ExpectedConditions.visibilityOf(scrollElement));
    }

    public void waitForScrollElementToBecomeHidden() {
        if(scrollElement.isDisplayed())
            wait.until(ExpectedConditions.invisibilityOf(scrollElement));
    }

    public boolean isLoaderPresent() { return scrollElement.isDisplayed(); }

    public String getMetaInfoValue(WebElement element,String field)
    {
        String result="";

        for (WebElement metainfo:element.findElements(By.className("search-result__meta-row"))) {
            if (metainfo.findElement(By.tagName("strong")).getText().equals(field)){
                result=metainfo.findElement(By.tagName("span")).getText().replaceAll("\\s+","");
                break;
            }
        }
        return result;
    }

    public void selectElementInResultsList(WebElement element)
    {
        actions.moveToElement(element).build().perform();
        wait.until(ExpectedConditions.visibilityOf(element.findElement(By.className("search-result__toggle--add"))));
        element.findElement(By.className("search-result__toggle--add")).click();
    }

    public void shortKeySelectElementInResultsList(WebElement element)
    {
        Keys keyToUse;
        if (System.getProperty("os.name").startsWith("Mac"))
            keyToUse = Keys.COMMAND;
        else
            keyToUse = Keys.LEFT_CONTROL;
        actions.keyDown(keyToUse)
                .click(element)
                .keyUp(keyToUse)
                .build()
                .perform();
    }

    public void shortKeySelectElementOnToggle(WebElement element)
    {
        Keys keyToUse;
        WebElement toggle=element.findElement(By.className("search-result__toggle--add"));
        if (System.getProperty("os.name").startsWith("Mac"))
            keyToUse = Keys.COMMAND;
        else
            keyToUse = Keys.LEFT_CONTROL;
        actions.moveToElement(element).build().perform();
        wait.until(ExpectedConditions.visibilityOf(toggle));

        actions.keyDown(keyToUse)
                .click(toggle)
                .keyUp(keyToUse)
                .build()
                .perform();
    }

    public void clickOnMetaArrow(WebElement result) {
        try {
            WebElement element = result.findElement(By.className("search-result__meta-toggle"));
            wait.until(ExpectedConditions.visibilityOf(element));
            actions.moveToElement(element).click().perform();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void selectSortingOrder(String sortingOrder) {
        if (!verifySortingOrderIsUnchanged(sortingOrder)) {
            switch (sortingOrder) {
                case "Most recent (import date)":
                    clickOnSortingOrderLink("CREATEDATE_DESCENDING");
                    break;
                case "Oldest (import date)":
                    clickOnSortingOrderLink("CREATEDATE_ASCENDING");
                    break;
                case "Most recent (shooting date)":
                    clickOnSortingOrderLink("SHOOTINGDATE_DESCENDING");
                    break;
                case "Oldest (shooting date)":
                    clickOnSortingOrderLink("SHOOTINGDATE_ASCENDING");
                    break;
            }
        }
    }

    private void clickOnSortingOrderLink(String value){
        WebElement filter = driver.findElement(By.xpath("//a[contains(@data-value,'"+value+"')]"));
        if(!filter.isDisplayed())
            click(sortingFilter);
        click(filter);
    }

    public boolean verifySortingOrderIsUnchanged(String sortingOrder)
    {
        return sortingFilter.getText().equalsIgnoreCase(sortingOrder);
    }

    public String getIdOfElement(WebElement element) { return getAttribute(element,"data-id"); }

    public int getNumberOfExtraSelectedImages(){
        return Integer.parseInt(getAttribute(selectionBannerImages,"data-extra-items-md").replace("+",""));
    }

    public void openFullView(WebElement element){
        actions.click(element.findElement(By.className("search-result__placeholder")))
                .build()
                .perform();
    }

    public void removeFirstElementFromSelection(){
        WebElement firstElem=driver.findElements(By.className("banner__preview")).get(0);
        WebElement deleteButton=driver.findElement(By.className("banner__preview-delete"));
        actions.moveToElement(firstElem).build().perform();
        actions.click(deleteButton).build().perform();
    }

    public boolean verifyMediaPoolofImage(int imageId,List<String> expectedMediapools){
        return MetadataUtils.getMediapoolNames(imageId)
                .stream()
                .anyMatch(
                        mediapool -> expectedMediapools.contains(mediapool)
                );
    }

    public boolean isContainingSmallImages() {
        return resultGrid.getAttribute("class").contains("search-results__grid--small");
    }

    public void closeFullView() {
        actions.click(driver.findElement(By.cssSelector("img.preview__close-icon")))
            .build().perform();
    }

    public WebElement getImageElement(int imageNr) {
        return results.get(imageNr).findElement(By.className("search-result__image"));
    }

    public void clickDownloadImages(){ click(downloadImagesButton); }

    public void selectElement(String action, WebElement elementToSelect) {
        if (action.contains("short-key"))
            if(action.contains("toggle"))
                shortKeySelectElementOnToggle(elementToSelect);
            else
                shortKeySelectElementInResultsList(elementToSelect);
        else
            selectElementInResultsList(elementToSelect);
    }
}
