package screens;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OneLoginScreen extends BaseScreen {

    @FindBy(id = "user_email")
    public WebElement emailInput;

    @FindBy(id = "user_password")
    public WebElement passwordInput;

    @FindBy(id = "user_submit")
    public WebElement loginButton;

    public OneLoginScreen(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public void login(String email,String password){
        enterText(emailInput,email);
        enterText(passwordInput,password);
        click(loginButton,5);
    }

}
