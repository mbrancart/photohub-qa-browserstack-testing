package screens;

import com.icafe4j.image.meta.Metadata;
import com.icafe4j.image.meta.iptc.IPTCApplicationTag;
import com.icafe4j.image.meta.iptc.IPTCDataSet;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import webdriver.WebDriverManagerUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly;
import static java.lang.String.format;
import static java.util.Arrays.stream;
import static java.util.concurrent.TimeUnit.SECONDS;

public class UploadScreen extends BaseScreen {

    private static final Log LOGGER = LogFactory.getLog(UploadScreen.class);

    public static final int CAPTION_SIZE_LIMIT = 250;
    WebDriver driver = WebDriverManagerUtil.getDriver();
    TabBanner tabBanner=new TabBanner(driver);
    List<String> captionList=new ArrayList<>();
    private static int expectedMatches;
    private static String captionToSearch;

    @FindBy(id = "files")
    public WebElement chooseFilesButton;

    @FindBy(css = "div.form-group.dragndrop-only")
    public WebElement dragAndDropArea;

    @FindBy(css = "body.layout.layout--split-view")
    public WebElement editDragAndDrop;

    @FindBy(id = "caption")
    public WebElement captionInput;

    @FindBy(className = "upload-edit__selected-count")
    public WebElement numberOfSelectedImages;

    //1 label and 1 button contain the same classname
    @FindBy(xpath = "//button[contains(@class,'button--block')]")
    public WebElement submitButton;

    @FindBy(css = "button.button.button--secondary")
    public WebElement selectAllButton;

    @FindBy(className = "button--primary")
    public WebElement finishButton;

    @FindBy(className = "button--secondary")
    public WebElement uploadButton;

    @FindBy(xpath = "//*[@class='uploads']//div[not(contains(@class,'hidden')) and not(contains(@class,'upload-edit--uploading'))]//div[contains(@class,'with-js')]\n")
    public List<WebElement> listUploadedPictures;

    @FindBy(xpath = "//*[@class='uploads']//div[contains(@class,'selected')]//div[contains(@class,'with-js')]\n")
    public List<WebElement> selectedImages;

    @FindBy(xpath = "//*[@class='uploads']//div[not(contains(@class,'selected'))]//div[contains(@class,'with-js')]\n")
    public List<WebElement> notSelectedImages;

    @FindBy(className = "upload-edit__placeholder")
    public List<WebElement> listPlaceholders;

    @FindBy(xpath = "//span[@class='upload-edit__count'][1]")
    public WebElement imageCount;

    @FindBy(className = "banner__error-message")
    public WebElement bannerErrorMessage;

    public UploadScreen(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
        driver.manage().timeouts().implicitlyWait(5000L, TimeUnit.MILLISECONDS);
        expectedMatches = 0;
    }

    public void uploadFiles(String[] listPaths) {
        doUploadFiles(getFilesFromPathAndUpdateExpectedMatches(listPaths));
    }

    public void uploadFilesWithShootingDate(String[] listPaths, String shootingDate) {
        final List<File> files = getFilesFromPathAndUpdateExpectedMatches(listPaths).stream()
                .map(file -> appendShootingDateToImageFile(file, shootingDate))
                .collect(Collectors.toList());
        doUploadFiles(files);
    }

    private void doUploadFiles(List<File> filesFromPath) {
        dropFiles(filesFromPath, dragAndDropArea, 0, 0);
        waitForUploadEdit();
    }

    public void addExtraFiles(String[] listPaths) {
        dropFiles(getFilesFromPathAndUpdateExpectedMatches(listPaths), editDragAndDrop, 0, 0);
        sleepUninterruptibly(4, SECONDS);
    }

    private List<File> getFilesFromPathAndUpdateExpectedMatches(String[] listPaths) {
        expectedMatches += listPaths.length;
        return getFilesFromPath(listPaths);
    }

    private List<File> getFilesFromPath(String[] listPaths) {
        return stream(listPaths)
                .map(inputFilePath -> getFile(inputFilePath))
                .collect(Collectors.toList());
    }

    private File getFile(String inputFile) {
        return new File("C:\\Users\\hello\\Desktop\\"+inputFile);
    }

    private File appendShootingDateToImageFile(File imageInputFile, String shootingDate) {
        String imageOutputFilePath = generateFilePathForImageOutputFile(imageInputFile);
        try(FileInputStream imageFileInputStream = new FileInputStream(imageInputFile);
            FileOutputStream fileOutputStream = new FileOutputStream(imageOutputFilePath)) {

            Metadata.insertIPTC(imageFileInputStream, fileOutputStream, createIPTCDataSetForShootingDate(shootingDate), true);
            return new File(imageOutputFilePath);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Appending shooting date failed", e);
        }
    }

    private String generateFilePathForImageOutputFile(File imageInputFile) {
        String filePath = imageInputFile.getPath();
        String fileExtension = "." + FilenameUtils.getExtension(filePath);
        return FilenameUtils.removeExtension(filePath)+ System.currentTimeMillis() + fileExtension;
    }

    private List<IPTCDataSet> createIPTCDataSetForShootingDate(String shootingDate) {
        return newArrayList(new IPTCDataSet(IPTCApplicationTag.DATE_CREATED, convertToIPTCDate(shootingDate)));
    }

    private String convertToIPTCDate(String dateWord) {
        Instant instant = createInstantNow();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        switch (dateWord) {
            case "today":
                return simpleDateFormat.format(Date.from(instant));
            case "yesterday":
                return simpleDateFormat.format(Date.from(instant.minus(1, ChronoUnit.DAYS)));
            default:
                throw new RuntimeException("convert to IPTC date failed because " + dateWord + " is not a valid option");
        }
    }

    private Instant createInstantNow() {
        Instant now = Instant.now();
        if(IsUtcTimeZone()) {
            now = addOneDay(now);
        }
        return now;
    }

    private Instant addOneDay(Instant now) {
        return now.plus(1, ChronoUnit.DAYS);
    }

    private boolean IsUtcTimeZone() {
        return TimeZone.getDefault().getOffset(System.currentTimeMillis()) == 0;
    }


    public void waitForUploadEdit() {
        new WebDriverWait(driver, 60).until((ExpectedCondition<Boolean>) webDriver ->
                webDriver.findElement(By.className("upload-edit__container-meta")).isDisplayed()
        );
    }

    public void fillInMetadataAndClickFinish(String caption, String photographer, String credits)
    {
        fillInMetaData(caption, photographer, credits);
        clickFinishMetadata(caption, credits);
    }

    public void fillInMetaData(String caption, String photographer, String credits) {
        for(int i=0;i<listUploadedPictures.size();i++){
            LOGGER.info("Filling out the caption of image " + i);
            doFillInMetaData(caption, photographer, credits, i);
        }
    }

    private void doFillInMetaData(String desiredCaption, String photographer, String credits, int imageNumber) {
        click(listUploadedPictures.get(imageNumber));
        if(StringUtils.isNotEmpty(desiredCaption))
            desiredCaption = String.format("ImageId %s - %s",getCurrentImageId(imageNumber),desiredCaption);
        captionList.add(desiredCaption);
        enterText(driver.findElement(By.name(format("photoAssets[%s].credit",imageNumber))),credits);
        //Still need to click on the field before starting writing
        driver.findElement(By.name(format("photoAssets[%s].caption",imageNumber))).click();
        enterText(driver.findElement(By.name(format("photoAssets[%s].caption",imageNumber))), desiredCaption);
        driver.findElement(By.name(format("photoAssets[%s].photographer",imageNumber))).click();
        enterText(driver.findElement(By.name(format("photoAssets[%s].photographer",imageNumber))),photographer);
    }

    private String getCurrentImageId(int imageNumber){
        return listUploadedPictures.get(imageNumber).findElement(By.xpath("..")).getAttribute("data-id");
    }

    public void addBrands(List<String> brands) {
        for(int i=0;i<listUploadedPictures.size(); i++){
            for(String brand : brands) {
                selectBrandForImage(brand, i);
            }
        }
    }

    private void selectBrandForImage(final String brand, final int imageNumber) {
        LOGGER.info(format("Select brand %s for image %s", brand, imageNumber));
        final WebElement brandCheckBox = clickOnImageAndGetCheckboxElement(brand, imageNumber);
        if(!getBrandCheckboxInput(imageNumber,brand).isSelected()) {
            scrollToAndClickBrandCheckBox(brandCheckBox);
            LOGGER.info(format("Selected brand %s for image %s", brand, imageNumber));
        }
    }

    private void scrollToAndClickBrandCheckBox(WebElement brandCheckBox) {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 1000)");
        brandCheckBox.click();
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0)");
    }

    public boolean removeBrandForImage(String brand, int imageNumber){
        LOGGER.info(format("Removing brand %s for image %s", brand, imageNumber));
        final WebElement brandCheckBox = clickOnImageAndGetCheckboxElement(brand, imageNumber);
        if(getBrandCheckboxInput(imageNumber,brand).isSelected()) {
            scrollToAndClickBrandCheckBox(brandCheckBox);
            LOGGER.info(format("Removed brand %s for image %s", brand, imageNumber));
        }
        return !brandCheckBox.isSelected();
    }

    public boolean areBrandsSelectedForImage(List<String> brands,int imageNumber){
        click(listUploadedPictures.get(imageNumber));
        return brands.stream()
                .allMatch(brand -> getBrandCheckboxInput(imageNumber, brand).isSelected());
    }

    //Selecting the input itself doesn't work. It's needed to get the span to be able to thick the checkbox
    private WebElement getBrandCheckboxComponent(int imageNumber, String brand) {
        return driver.findElement(By.xpath(String.format("//div[@class=\"layout__sidebar\"]//*[@id=\"photoAssets%s.brands.%s\"]/following-sibling::span", imageNumber, brand)));
    }

    private WebElement getBrandCheckboxInput(int imageNumber, String brand) {
        return driver.findElement(By.xpath(String.format("//div[@class=\"layout__sidebar\"]//*[@id=\"photoAssets%s.brands.%s\"]", imageNumber, brand)));
    }

    private WebElement clickOnImageAndGetCheckboxElement(String brand, int imageNumber) {
        click(listUploadedPictures.get(imageNumber));

        final WebElement brandCheckBox = getBrandCheckboxComponent(imageNumber,brand);
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(brandCheckBox));
        return brandCheckBox;
    }

    public void clickFinishMetadata(String caption, String credits) {
        if(!StringUtils.isBlank(caption)&&!StringUtils.isBlank(credits))
            clickFinish();
    }

    public void verifyUploadPresentInResultScreen() {
        verifyUploadPresentInResultScreen(true);
    }

    public void verifyUploadPresentInResultScreen(boolean refreshResultScreen) {
        ResultScreen resultScreen=new ResultScreen(driver);
        int matches = countImagesWithUploadedCaptions(resultScreen, refreshResultScreen);
        Assert.assertEquals("didn't find the correct amount of matches",expectedMatches, matches);
    }

    public void verifyUploadNotPresentInResultScreen(boolean refreshResultScreen) {
        ResultScreen resultScreen=new ResultScreen(driver);
        int matches = countImagesWithUploadedCaptions(resultScreen, refreshResultScreen);
        Assert.assertEquals("Found an unexpected match",0, matches);
    }

    private int countImagesWithUploadedCaptions(ResultScreen resultScreen, boolean refreshResultScreen) {
        if (refreshResultScreen) {
            new Actions(driver)
                    .click(tabBanner.searchTab)
                    .build().perform();
        }
        return (int) resultScreen.getResultList().stream()
                .map(asset -> asset.findElement(By.className("search-result__image")).findElement(By.tagName("source")).getAttribute("alt"))
                .filter(this::captionListContainsCaption)
                .count();
    }

    private boolean captionListContainsCaption(String captionToFind){
        return captionList.stream()
                .anyMatch(aCaption -> aCaption.equalsIgnoreCase(captionToFind));
    }

    public String getCaptionOfElement(int positionInList){
        return driver.findElement(By.name(format("photoAssets[%s].caption",positionInList))).getAttribute("value");
    }

    public String getPhotographerOfElement(int positionInList){
        return driver.findElement(By.name(format("photoAssets[%s].photographer",positionInList))).getAttribute("value");
    }

    public String getCreditsOfElement(int positionInList){
        return driver.findElement(By.name(format("photoAssets[%s].credit",positionInList))).getAttribute("value");
    }

    public void clickSubmit() {click(submitButton,5); }

    public boolean isSubmitButtonDisabled() {
        return finishButton.getAttribute("class").contains("button--disabled");
    }

    public String getCaptionToSearch(){ return captionToSearch; }
    public void setCaptionToSearch(String value){captionToSearch=value;}

    public void dropFiles(String[] filePaths) {
        List<File> files = getFiles(filePaths);
        dropFiles(files, dragAndDropArea, 0, 0);
    }

    public List<File> getFiles(String[] filePaths) {
        ClassLoader classLoader = getClass().getClassLoader();
        return stream(filePaths)
                .map(fp -> new File(classLoader.getResource(fp).getFile()))
                .collect(Collectors.toList());
    }

    public void dropFiles(List<File> filePaths, WebElement target, int offsetX, int offsetY) {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        WebDriverWait wait = new WebDriverWait(driver, 30);

        String JS_DROP_FILE =
                "var target = arguments[0]," +
                        "    offsetX = arguments[1]," +
                        "    offsetY = arguments[2]," +
                        "    document = target.ownerDocument || document," +
                        "    window = document.defaultView || window;" +
                        "" +
                        "var input = document.createElement('INPUT');" +
                        "input.type = 'file';" +
                        "input.multiple = 'multiple';" +
                        "input.style.display = 'none';" +
                        "input.onchange = function () {" +
                        "  var rect = target.getBoundingClientRect()," +
                        "      x = rect.left + (offsetX || (rect.width >> 1))," +
                        "      y = rect.top + (offsetY || (rect.height >> 1))," +
                        "      dataTransfer = { files: this.files };" +
                        "" +
                        "  ['dragenter', 'dragover', 'drop'].forEach(function (name) {" +
                        "    var evt = document.createEvent('MouseEvent');" +
                        "    evt.initMouseEvent(name, !0, !0, window, 0, 0, 0, x, y, !1, !1, !1, !1, 0, null);" +
                        "    evt.dataTransfer = dataTransfer;" +
                        "    target.dispatchEvent(evt);" +
                        "  });" +
                        "" +
                        "  setTimeout(function () { document.body.removeChild(input); }, 25);" +
                        "};" +
                        "document.body.appendChild(input);" +
                        "return input;";

        WebElement input =  (WebElement)jse.executeScript(JS_DROP_FILE, target, offsetX, offsetY);
        //building string as follows: sendKeys("path/to/first/file-001 \n path/to/first/file-002 \n path/to/first/file-003")
        String collect = filePaths.stream()
                .map(file -> file.toString())
                .collect(Collectors.joining("\n"))
                .trim();
        input.sendKeys(collect);
        wait.until(ExpectedConditions.stalenessOf(input));
    }

    public boolean deleteLastImage(){
        clickDeleteImageIconOfImage(listUploadedPictures.size()-1);
        return listUploadedPictures.size()==expectedMatches ;
    }

    public boolean deleteImageWithIndex(int index) {
        clickDeleteImageIconOfImage(index);
        return listUploadedPictures.size() == expectedMatches;
    }

    public void clickDeleteImageIconOfImage(int index) {
        listUploadedPictures.get(index).findElement(By.className("upload-edit__delete-icon")).click();
        expectedMatches--;
    }

    public int getImageCount(){
        return Integer.parseInt(driver.findElement(By.cssSelector("span.upload-edit__count")).getText());
    }

    public int getExpectedImagesOnPage(){
        return expectedMatches;
    }

    public String getErrorBannerText(){
        return bannerErrorMessage.getText();
    }

    public String getDisplayedCaptionOfPreview(int previewPosition){
        return listUploadedPictures.get(previewPosition-1).findElement(By.className("upload-edit__description")).getText();
    }

    public boolean verifyCaptionToggleVisible(int position) {
        return !listUploadedPictures.get(position -1)
                .findElement(By.className("description__caption--toggle"))
                .getAttribute("class").contains("hidden");
    }

    public boolean verifyUploadZonePresent() {
        return driver.findElement(By.className("upload-zone")) != null;
    }

    public boolean verifyUploadErrorModalIsPresent() {
        new WebDriverWait(driver, 60)
                .until((ExpectedCondition<Boolean>) webDriver
                        -> getErrorModalAriaAttribute() != null);
        return getErrorModalAriaAttribute().contains("false");
    }

    private String getErrorModalAriaAttribute() {
        return driver.findElement(By.id("error__modal")).getAttribute("aria-hidden");
    }

    public void clickSelectAllImages() {
        new Actions(driver)
                .click(selectAllButton)
                .build()
                .perform();
    }

    public void fillInMetaDataForOneImage(String caption, String photographer, String credit) {
        enterText(driver.findElement(By.xpath("//input[contains(@name, 'caption')]")), caption);
        enterText(driver.findElement(By.xpath("//input[contains(@name, 'photographer')]")), photographer);
        enterText(driver.findElement(By.xpath("//input[contains(@name, 'credit')]")), credit);
    }

    public void fillInMetaDataForAllImages(String caption, String photographer, String credit) {
        clickSelectAllImages();
        enterText(driver.findElement(By.xpath("//input[contains(@name, 'caption')]")), caption);
        captionList.add(caption);
        enterText(driver.findElement(By.xpath("//input[contains(@name, 'photographer')]")), photographer);
        enterText(driver.findElement(By.xpath("//input[contains(@name, 'credit')]")), credit);
    }

    public void selectImages(int amountOfImagesToSelect) {
        Actions actions = new Actions(driver);
        Keys multiSelect = Keys.COMMAND;
        actions.keyDown(multiSelect);
        for(int i = 0; i< amountOfImagesToSelect ; i++) {           //first image is selected
            actions.click(listUploadedPictures.get(i));
        }
        actions.keyUp(multiSelect)
            .build().perform();
    }

    public void uploadAdditionalFiles(String imagePath) {
        List<File> files = getFiles(new String[]{imagePath});
        expectedMatches += files.size();
        dropFiles(files, editDragAndDrop,0, 0 );
    }

    public void clickFinish() {
        new Actions(driver)
                .click(finishButton)
                .build().perform();
    }


    public void waitForImagesToBeDisplayed(){
        //120 is the timeout for the upload
        new WebDriverWait(driver,120).until(
                (ExpectedCondition<Boolean>) driver -> {
                    int elementsOnPage= this.listUploadedPictures.size();
                    if(elementsOnPage==this.getExpectedImagesOnPage())
                        return true;
                    else
                        return false;
                }
        );
    }

    public boolean isPlaceholderDisplayedForAllImages(){
        return listPlaceholders.stream()
                .allMatch(placeholder -> isValidImageSource(placeholder));
    }

    private boolean isValidImageSource(WebElement placeholder) {
        String source = placeholder.findElement(By.tagName("img")).getAttribute("src");
        return source.contains("data:image/")||source.contains("data:application/pdf");
    }

    public boolean isNumberOfErrorsCorrect(int expectedNumberOfErrors) {
        return new WebDriverWait(driver,120).until(
                (ExpectedCondition<Boolean>) driver -> {
                    int elementsOnPage= driver.findElements(By.xpath("//div[@class='uploads']/div[contains(@class,'error')]")).size();
                    if(elementsOnPage==expectedNumberOfErrors)
                        return true;
                    else
                        return false;
                }
        );
    }

    public boolean isBrandChoiceRestricted(List<String> expectedBrands) {
        return getCheckboxes().containsAll(expectedBrands);
    }

    private List<String> getCheckboxes() {
        List<WebElement> checkboxList= driver.findElements(By.className("checkbox"));
        return checkboxList.subList(0,checkboxList.size()/2).stream().map(WebElement::getText).collect(Collectors.toList()); //the actual number of elements found is always X2
    }
}
