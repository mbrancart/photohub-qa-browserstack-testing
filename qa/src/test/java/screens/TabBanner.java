package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import webdriver.WebDriverManagerUtil;

public class TabBanner extends BaseScreen {

    WebDriver driver = WebDriverManagerUtil.getDriver();

    @FindBy(xpath = "//a[contains(@href,'/search')]")
    public WebElement searchTab;

    @FindBy(xpath = "//a[contains(@href,'/upload')]")
    public WebElement uploadTab;

    @FindBy(xpath = "//a[contains(@href,'/assignments')]")
    public WebElement assignmentsTab;

    @FindBy(xpath = "//a[contains(@href,'/bookmarks')]")
    public WebElement bookmarksTab;

    public TabBanner(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public void openSearchTab() { click(searchTab,5); }

    public void openUploadTab() { click(uploadTab,5); }

    public void openBookmarksTab() {click(bookmarksTab,5);}

    public void openAssignmentsTab() { click(assignmentsTab,5); }

    public boolean verifyUploadTabOpened(){
         return getAttribute(driver.findElement(By.xpath("//a[contains(@href,'/upload')]")),"class").contains("active");
    }

    public boolean verifySearchTabOpened(){
        return getAttribute(driver.findElement(By.xpath("//a[contains(@href,'/search')]")),"class").contains("active");
    }

    public boolean verifyAssignmentsTabOpened(){
        return getAttribute(driver.findElement(By.xpath("//a[contains(@href,'/assignments')]")),"class").contains("active");
    }

    public boolean verifyBookmarksTabOpened(){
        return getAttribute(driver.findElement(By.xpath("//a[contains(@href,'/bookmarks')]")),"class").contains("active");
    }
}
