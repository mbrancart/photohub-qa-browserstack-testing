package screens;

import net.persgroep.diocontent.images.api.asset.Asset;
import net.persgroep.diocontent.images.api.asset.ImageInfo;
import net.persgroep.diocontent.images.api.client.DioContentClient;
import net.persgroep.diocontent.images.client.DefaultRestDioContentClient;
import org.apache.commons.lang.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Metadata;
import webdriver.WebDriverManagerUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class FullViewScreen extends BaseScreen {

    WebDriverWait wait;
    private Actions actions;

    private DioContentClient dioContent=null;
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private Asset asset=null;
    private ImageInfo imageInfo = null;

    public FullViewScreen(WebDriver driver) {
        super(driver);
        wait = new WebDriverWait(driver,5);
        actions=new Actions(driver);
        PageFactory.initElements(driver,this);
    }

    @FindBy(className = "preview__image")
    public WebElement previewImage;

    @FindBy(className = "preview__description")
    public WebElement caption;

    @FindBy(className = "preview__header-title")
    public WebElement imageTitle;

    @FindBy(className = "preview__select-button")
    public WebElement btnSelectPicture;

    @FindBy(className = "preview__close-icon")
    public WebElement btnClose;

    @FindBy(className = "preview__arrow--next")
    public WebElement nextImageArrow;

    @FindBy(className = "preview__arrow--previous")
    public WebElement previousImageArrow;

    @FindBy(className = "preview__meta-item--price-range")
    public WebElement priceRange;

    @FindBy(className = "fjs_single_download_form")
    public WebElement downloadSingleImageButton;

    public boolean isFullViewOpened(){
        return isOnPage(btnSelectPicture,5);
    }

    public void closeFullView(){ click(btnClose,5); }

    public void addPictureToSelection(){
        click(btnSelectPicture,5);
        //selection seems to fail from time to time
        if(!btnSelectPicture.getText().equalsIgnoreCase("Deselect picture")) {
            System.out.println("picture is not selected, pushing the select button again.");
            click(btnSelectPicture, 5);
        }
    }

    public void removePictureToSelection() {
        click(btnSelectPicture,5);
        //selection seems to fail from time to time
        if(!btnSelectPicture.getText().equalsIgnoreCase("Select picture")) {
            System.out.println("picture is still selected, pushing the deselect button again.");
            click(btnSelectPicture, 5);
        }
    }

    public String getMetaInfoValue(WebElement element,String field)
    {
        String result="";

        for (WebElement metainfo:element.findElements(By.className("preview__meta-row"))) {
            if (metainfo.findElement(By.tagName("strong")).getText().equals(field)){
                result=metainfo.findElement(By.tagName("span")).getText().replaceAll("\\s+","").replaceAll("\\u00a0","");
                break;
            }
        }
        return result;
    }

    public String getCaption(){
        return caption.getText();
    }

    public String getImageSource() {
        return previewImage.getAttribute("src");
    }

    public void clickNext() {
        try {
            wait.until(ExpectedConditions.visibilityOf(nextImageArrow));
            actions.moveToElement(nextImageArrow).click().perform();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void clickPrevious() {
        try {
            wait.until(ExpectedConditions.visibilityOf(previousImageArrow));
            actions.moveToElement(previousImageArrow).click().perform();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void clickDownloadImage(){
        click(downloadSingleImageButton,5);
    }

    public List<Metadata> selectImagesAndGetMetaInfo(int numberOfImages) {
        List<Metadata> listMetaFrontend = new ArrayList<>();
        for(int i = 0; i < numberOfImages; i++) {
            WebElement currentImage=driver.findElement(By.className("preview"));
            listMetaFrontend.add(new Metadata(getCaption(),getMetaInfoValue(currentImage,"Credits")));
            clickNext();
            addPictureToSelection();
        }
        return listMetaFrontend;
    }
}
