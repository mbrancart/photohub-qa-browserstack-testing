package screens.searchComponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import screens.BaseScreen;

public class SearchHeader extends BaseScreen {

    public SearchHeader(WebDriver driver) {
        super(driver);
    }

    public void selectPageSize(String pageSizeValue) {
        WebElement filter = driver.findElement(By.xpath("//a[contains(@data-value,'"+ pageSizeValue +"')]"));
        if(!filter.isDisplayed())
            click(driver.findElement(By.xpath("//label[contains(@for, 'dropdown__toggle-size')]")));
        if(!filter.getAttribute("class").contains("link-list__item--active"))
            click(filter);
    }

}
