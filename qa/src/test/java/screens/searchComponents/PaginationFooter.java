package screens.searchComponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import screens.BaseScreen;

import java.util.List;

public class PaginationFooter extends BaseScreen {

    public WebElement prevPageButton;
    public WebElement nextPageButton;
    public WebElement index;
    private int initialIndex;

    public PaginationFooter(WebDriver driver) {
        super(driver);
        WebElement paginationDiv = driver.findElement(By.className("pagination"));

        this.prevPageButton = paginationDiv.findElement(By.xpath("//i[contains(@class, 'fi-dropdown--left')]/.."));
        this.nextPageButton = paginationDiv.findElement(By.xpath("//i[contains(@class, 'fi-dropdown--right')]/.."));
        this.index = paginationDiv.findElement(By.className("pagination__picker__input")).findElement(By.className("input"));
        this.initialIndex = Integer.parseInt(index.getAttribute("value"));
    }

    public void goToNextPage() {
        click(nextPageButton);
    }

    public int getIndex() {
        return initialIndex;
    }

    public void setIndex(String requestedIndex) {
       enterText(this.index, requestedIndex);
    }

    public void setInintialIndex(int inintialIndex) {
        this.initialIndex = inintialIndex;
    }

    public int getMaxAmountOfPages() {
        WebElement paginationDiv = driver.findElement(By.className("pagination"));
        return Integer.parseInt(paginationDiv.findElement(By.className("pagination__picker__amount")).getText());
    }
}
