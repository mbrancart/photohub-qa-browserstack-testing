package screens;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BookmarksScreen extends BaseScreen {

    private WebDriverWait wait;

    public BookmarksScreen(WebDriver driver) {
        super(driver);
        wait= new WebDriverWait(driver,10);
        PageFactory.initElements(driver,this);
    }


    @FindBy(className = "folders__submit-button")
    private WebElement loadFolderButton;

    @FindBy(className = "search-results__query")
    private WebElement pageTitle;

    @FindBy(xpath = "//div[@class='pagination__buttons']/button")
    private WebElement nextPageButton;

    @FindBy(className = "search-result")
    private List<WebElement> bookmarks;


    public void openFolder(String folderPath){
        openParentFolders(folderPath);
        click(driver.findElement(By.xpath(getFolderLinkXpath(getTargetFolderName(folderPath)))));
        click(loadFolderButton);
    }

    private void openParentFolders(String folderPath) {
        String[] folders= getFoldersTable(folderPath);
        for(int i=0;i<folders.length-1;i++){
            clickOnArrowBasedOnFolderName(folders[i]);
        }
    }

    private String[] getFoldersTable(String folderPath) {
        return StringUtils.split(folderPath, "/");
    }

    private boolean isSubfolder(String bookmarkName) {
        return driver.findElement(By.xpath(getFolderLinkXpath(bookmarkName)+"/../../..")).getAttribute("class").equalsIgnoreCase("folder__subfolders");
    }

    public void clickOnArrowBasedOnFolderName(String bookmarkName)
    {
        click(driver.findElement(By.xpath(getFolderLinkXpath(bookmarkName)+"/../preceding-sibling::label/span")));
    }

    private List<WebElement> getSubFoldersOfParent(String parentFolderName){
        return driver.findElements(By.xpath(getFolderLinkXpath(parentFolderName)+"/../following-sibling::ul/li"));
    }

    private String getFolderLinkXpath(String bookmarkName){
        return "//span[text()='"+bookmarkName+"']";
    }

    public boolean isFolderNameInBold(String folderPath){
        return StringUtils.equalsIgnoreCase(driver.findElement(By.xpath(getFolderLinkXpath(getTargetFolderName(folderPath)))).getCssValue("font-weight"),"700");
    }

    public boolean isTitleContainingFolderName(String folderPath){
        return StringUtils.contains(pageTitle.getText(),getTargetFolderName(folderPath));
    }

    public boolean areSubfoldersVisible(String parentFolderName){
        return getSubFoldersOfParent(parentFolderName).stream().allMatch(subfolder -> subfolder.isDisplayed());
    }

    private String getTargetFolderName(String folderName) {
        String[] folders= getFoldersTable(folderName);
        return folders[folders.length-1];
    }

    public void clickOnNextPage(){
        click(nextPageButton);
    }

    private int getAmountBookmarksInFolder(){
        return Integer.parseInt(pageTitle.getText().substring(pageTitle.getText().lastIndexOf("(")+1,pageTitle.getText().lastIndexOf(")")));
    }

    public int getExpectedBookmarksOnPage(){
        int pagesize = Integer.parseInt(getQueryParameterValue("size"));
        int pageIndex = Integer.parseInt(getQueryParameterValue("index"));
        return getAmountBookmarksInFolder()-(pagesize*(pageIndex-1));
    }

    public int getActualNumberBookmarksOnPage(){
        return bookmarks.size();
    }




}
