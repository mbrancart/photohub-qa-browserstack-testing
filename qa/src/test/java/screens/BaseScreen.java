package screens;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class BaseScreen {
    protected WebDriver driver;
    private static final Log LOGGER = LogFactory.getLog(BaseScreen.class);

    public BaseScreen(WebDriver driver) {
        this.driver = driver;
    }

    public void click(WebElement element, int... timeOutInSeconds){
        synchronized (driver){
            try {
                int timeout = 15;
                if(timeOutInSeconds.length > 0){
                    timeout = timeOutInSeconds[0];
                }
                waitForElementToBeClickable(element, timeout);
                element.click();
                driver.wait(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void enterText(WebElement element, String text){
        waitForElementToBeVisible(element, 15);
        element.clear();
        element.sendKeys(text);
    }

    public String readText(WebElement element, int... timeOutInSeconds){
        int timeout = 15;
        if(timeOutInSeconds.length > 0){
            timeout = timeOutInSeconds[0];
        }
        waitForElementToBeVisible(element, timeout);
        return element.getText();
    }

    public boolean isOnPage(WebElement element, int... timeOutInSeconds){
        int timeout = 15;
        if(timeOutInSeconds.length > 0){
            timeout = timeOutInSeconds[0];
        }
        waitForElementToBeVisible(element, timeout);
        return element.isDisplayed();
    }

    public void waitForElementToBeVisible(WebElement element, int timeOutInSeconds){
        ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
            @Nullable
            @Override
            public Boolean apply(@Nullable WebDriver webDriver) {
                try{
                    return element.isDisplayed();
                }
                catch(NoSuchElementException nse){
                    return null;
                }
                catch(WebDriverException wbde){
                    LOGGER.warn("A WebDriverException was caught with message: " + wbde.getLocalizedMessage());
                    return null;
                }
            }
        };
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        wait.until(condition);
    }

    public void waitForElementToBeClickable(WebElement element, int... timeOutInSeconds){
        int timeout = 15;
        if(timeOutInSeconds.length > 0){
            timeout = timeOutInSeconds[0];
        }
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void switchToFrame(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
    }

    public String getAttribute(WebElement element, String attribute){
        ExpectedCondition<Boolean> condition = new ExpectedCondition<Boolean>() {
            @Nullable
            @Override
            public Boolean apply(@Nullable WebDriver webDriver) {
                try{
                    return !element.getAttribute(attribute).isEmpty();
                }
                catch(NoSuchElementException nse){
                    return null;
                }
                catch(WebDriverException wbde){
                    LOGGER.warn("A WebDriverException was caught with message: " + wbde.getLocalizedMessage());
                    return null;
                }
            }
        };
        WebDriverWait wait = new WebDriverWait(driver, 5);
        return element.getAttribute(attribute);
    }

    public void takeScreenshot(){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            Date date = new Date();
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            System.out.println("taking screenshot with name : "+ dateFormat.format(date));
            FileUtils.copyFile(scrFile, new File("src/test/resources/screenshots/" + dateFormat.format(date) + ".jpg"));
        }catch (IOException ex){
            LOGGER.warn("could not take screenshot: "+ex.getLocalizedMessage());
        }
    }


    public Date getDateFromString(String date){
        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public String getStringFromDate(String date){
        return String.format(getLocalDateFromString(date).toString());
    }

    public LocalDate getLocalDateFromString(String date){
        return LocalDate.parse(date.substring(0,10), DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }



    public void scrollToTop(){
        javascriptExecutor().executeScript("scrollTo(0,0)");
    }

    protected JavascriptExecutor javascriptExecutor() {
        return (JavascriptExecutor) driver;
    }

    public List<String> getQueryParameters() throws MalformedURLException {
        String query = new URL(driver.getCurrentUrl()).getQuery();
        if(query!=null)
            return Arrays.asList(query.replaceAll("%3F","").split("&"));
        return new ArrayList<>();
    }

    public void zoomIn(int percentage){
        javascriptExecutor().executeScript("document.body.style.zoom='"+percentage+"%'");
    }

    public String getQueryParameterValue(String parameterName){
        List<NameValuePair> params;
        try {
            params = URLEncodedUtils.parse(new URI(driver.getCurrentUrl()), "UTF-8");
            return params
                    .stream()
                    .filter(param->param.getName().equals(parameterName))
                    .findFirst()
                    .get()
                    .getValue();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return "";
    }

}
