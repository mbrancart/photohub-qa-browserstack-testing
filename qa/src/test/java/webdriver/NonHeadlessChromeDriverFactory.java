package webdriver;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.HashMap;

public class NonHeadlessChromeDriverFactory {

    private static final Log LOGGER = LogFactory.getLog(NonHeadlessChromeDriverFactory.class);

    public ChromeDriver create(String downloadFolder) {
        LOGGER.info("Starting WebDriver with Chrome");
        return new ChromeDriver(getChromeOptions(downloadFolder));
    }

    private static ChromeOptions getChromeOptions(String downloadFolder) {
        ChromeOptions options = new ChromeOptions();
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", downloadFolder);
        options.setExperimentalOption("prefs",chromePrefs);
        return options;
    }
}
