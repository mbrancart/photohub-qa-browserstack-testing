package webdriver;

import cucumber.api.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import net.lightbody.bmp.BrowserMobProxy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static javax.swing.plaf.synth.SynthConstants.DISABLED;
import static org.apache.commons.lang.StringUtils.isNotBlank;
import static webdriver.HeadlessChromeDriverFactory.URL;

public class WebDriverManagerUtil {

    /**
     * This class provides utilities to manage WebDrivers. It helps to create, start, reuse and close WebDriver instances. Dependencies:
     * <p>
     * io.github.bonigarcia webdrivermanager
     * <p>
     * org.seleniumhq.selenium selenium-java
     */

    private static final WebDriverManagerUtil instance = new WebDriverManagerUtil();
    public static WebDriver driver = null;
    private static final String link;
    private static final String standaloneLink;
    public static BrowserMobProxy server;
    private static final Log LOGGER = LogFactory.getLog(WebDriverManagerUtil.class);
    private static ChromeDriverFactory chromeDriverFactory = new ChromeDriverFactory();
    public static String downloadFolder;

    static {
        String photohubLinkFromSystemProperty = System.getProperty("photohub.url");
        if(isNotBlank(photohubLinkFromSystemProperty)) {
            link = photohubLinkFromSystemProperty;
            LOGGER.info("Running tests for for photohub url " + link);
        } else {
            link = WebDriverLink.getDefaultLink();
            LOGGER.info("No url configured, running tests for for default photohub url " + link);
        }

        String photohubStandaloneLinkFromSystemProperty = System.getProperty("photohub.standalone.url");
        if(isNotBlank(photohubStandaloneLinkFromSystemProperty)) {
            standaloneLink = photohubStandaloneLinkFromSystemProperty;
        } else {
            standaloneLink = WebDriverLink.getDefaultLink();
        }

        LOGGER.info("Running tests in timezone: " + TimeZone.getDefault());
    }

    public static String getLink() {
        return link;
    }

    public static String getStandaloneLink() {
        return standaloneLink;
    }

    public static WebDriverManagerUtil getInstance() {
        return instance;
    }

    public static String getBrowser() {
        LOGGER.info("Retrieving browser properties");
        String browser = System.getProperty("browser");

        if (browser == null)
            browser = "chrome";
        return browser;
    }

    public static WebDriver getDriver()
    {
        return driver;
    }

    public static WebDriver setupDriver(Scenario scenario) {
        downloadFolder=createDownloadFolderName();

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browser", "Chrome");
        caps.setCapability("browser_version", "49.0");
        caps.setCapability("os", "Windows");
        caps.setCapability("os_version", "10");
        caps.setCapability("resolution", "1024x768");
        caps.setCapability("project", "Photohub testing");
        caps.setCapability("name", "Photohub testing - "+ scenario.getName());
        caps.setCapability("browserstack.debug",true);
        caps.setCapability("browserstack.networkLogs",true);
        caps.setCapability("browserstack.sendKeys","true");
        caps.setCapability("fixSessionCapabilities",true);
        caps.setCapability("remoteFiles",true);



        Map<String, Object> chromePrefs = new HashMap<>();
        //chromePrefs.put("profile.managed_default_content_settings.javascript", 2);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        options.setCapability("browser", "Chrome");
        options.setCapability("browser_version", "49.0");
        options.setCapability("os", "Windows");
        options.setCapability("os_version", "10");
        options.setCapability("resolution", "1024x768");
        options.setCapability("project", "Photohub testing");
        options.setCapability("name", "Photohub testing - "+ scenario.getName());
        options.setCapability("browserstack.debug",true);
        options.setCapability("browserstack.networkLogs",true);
        options.setCapability("browserstack.sendKeys","true");
        options.setCapability("fixSessionCapabilities",true);
        options.setCapability("remoteFiles",true);

        options.setCapability("browser.download.folderList", 0);
        options.setCapability("browser.download.manager.showWhenStarting", false);
        options.setCapability("browser.download.manager.focusWhenStarting", false);
        options.setCapability("browser.download.useDownloadDir", true);
        options.setCapability("browser.helperApps.alwaysAsk.force", false);
        options.setCapability("browser.download.manager.alertOnEXEOpen", false);
        options.setCapability("browser.download.manager.closeWhenDone", true);
        options.setCapability("browser.download.manager.showAlertOnComplete", false);
        options.setCapability("browser.download.manager.useWindow", false);
        /* you will need to find the content-type of your app and set it here. */
        options.setCapability("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream");


        try {
            driver = new RemoteWebDriver(new URL(URL), options);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Locale.setDefault(new Locale(Locale.FRANCE.getLanguage(),Locale.FRANCE.getCountry()));

        driver.get(link);

        return driver;
    }

    private static String createDownloadFolderName(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        LocalDateTime now = LocalDateTime.now();
        return Paths.get("").toAbsolutePath().toString()+"/src/test/resources/downloads/downloads_"+formatter.format(now);
    }

    public void removeDriver() {
        LOGGER.info("Closing WebDriver");
        driver.close();

        LOGGER.info("Quitting WebDriver Background Processes");
        driver.quit();
    }

    /**
     * Fallback urls if no system property was configured for the photohub endpoint.
     */
    private static class WebDriverLink {

        private static final String LOCAL_URL = "http://localhost:8080/";
        private static final String LOCAL_URL_STANDALONE = "http://localhost:8080/";
        private static final String DEV_URL = "https://photohub.dev.picture.persgroep.cloud/";
        private static final String DEV_URL_STANDALONE = "https://photohub.standalone.dev.picture.persgroep.cloud/";
        private static final String PROD_URL = "https://photohub.picture.persgroep.cloud/";
        private static final String PROD_URL_STANDALONE = "https://photohub.standalone.picture.persgroep.cloud/";
        private static final String NON_PROD_URL = "https://photohub.non-prod.picture.persgroep.cloud/";
        private static final String NON_PROD_URL_STANDALONE = "https://photohub.standalone.non-prod.picture.persgroep.cloud/";

        private static String getDefaultLink() {
            return DEV_URL;
        }

        private static String getDefaultStandaloneLink() {
            return LOCAL_URL_STANDALONE;
        }
    }
}
