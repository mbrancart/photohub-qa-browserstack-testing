package webdriver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeDriverFactory {

    public ChromeDriverFactory() {
        setupChromeDriver();
    }

    private static void setupChromeDriver() {
        WebDriverManager.chromedriver().setup();
    }

    public ChromeDriver createHeadless(String downloadFolder) {
       return new HeadlessChromeDriverFactory().create(downloadFolder);
    }

    public ChromeDriver createDefault(String downloadFolder) {
        return new NonHeadlessChromeDriverFactory().create(downloadFolder);
    }

}
