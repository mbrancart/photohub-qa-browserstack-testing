package webdriver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HeadlessChromeDriverFactory {

    private static final Log LOGGER = LogFactory.getLog(HeadlessChromeDriverFactory.class);

    private static final boolean PIPELINE_CONFIGURATION_ENABLED_DEFAULT = false;
    public static final String USERNAME = "persgroeppicture1";
    public static final String AUTOMATE_KEY = "7p3SV4wQLTy53LhxRkjw";
    public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    private final boolean isPipelineConfigurationEnabled;

    public HeadlessChromeDriverFactory() {
        isPipelineConfigurationEnabled = getPipelineEnabledConfiguration();
    }

    private boolean getPipelineEnabledConfiguration() {
        String configurationProperty = System.getProperty("pipelineconfiguration.enabled");
        if (configurationProperty != null) {
            return Boolean.valueOf(configurationProperty);
        }
        return PIPELINE_CONFIGURATION_ENABLED_DEFAULT;
    }

    public ChromeDriver create(String downloadFolder) {
        LOGGER.info("Starting WebDriver with Headless Chrome");
        ChromeDriver driver;

        ChromeOptions options = getChromeOptions();
        ChromeDriverService driverService = ChromeDriverService.createDefaultService();
        // This seems to fail occasionally because of an error in chromedriver/chrome
        try{
            driver = new ChromeDriver(driverService, options);
        }
        catch(SessionNotCreatedException ex){
            driver = new ChromeDriver(driverService, options);
        }
        enableFileDownloadInHeadlessMode(driverService, driver, downloadFolder);
        return driver;
    }

    private ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-sandbox" );

        options.addArguments("--headless" );
        options.addArguments("--disable-gpu");
        options.addArguments("--whitelisted-ips=\"\"" );
        options.addArguments("--disable-dev-shm-usage" );
        options.addArguments("--enable-features=NetworkService,NetworkServiceInProcess");
        options.addArguments("--lang=en-GB");

        if(isPipelineConfigurationEnabled) {
            //options.setBinary("/usr/bin/google-chrome");
        }

        return options;
    }

    private HashMap<String, Object> createChromePreferences(String downloadFolder) {
        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("behavior","allow");
        chromePrefs.put("downloadPath", downloadFolder);
        return chromePrefs;
    }

    private void enableFileDownloadInHeadlessMode(ChromeDriverService driverService, ChromeDriver driver, String downloadFolder) {
        try {
            tryEnableFileDownloadInHeadlessMode(driverService, driver, downloadFolder);
        } catch (IOException e) {
            LOGGER.error("failed to enable headless file download");
            e.printStackTrace();
        }
    }

    private void tryEnableFileDownloadInHeadlessMode(ChromeDriverService driverService, ChromeDriver driver,  String downloadFolder) throws IOException {
        String command = createCommandToEnableDownload(downloadFolder);
        String url = driverService.getUrl().toString() + "/session/" + driver.getSessionId() + "/chromium/send_command";
        executeCommand(command, url);
    }

    private String createCommandToEnableDownload(String downloadFolder) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(createDownloadCommandParams(downloadFolder));
    }

    private Map<String,Object> createDownloadCommandParams(String downloadFolder){
        Map<String, Object> commandParams = new HashMap<>();
        commandParams.put("cmd", "Page.setDownloadBehavior");
        commandParams.put("params", createChromePreferences(downloadFolder));
        return commandParams;
    }

    private void executeCommand(String command, String url) throws IOException {
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(url);
        request.addHeader("content-type", "application/json");
        request.setEntity(new StringEntity(command));
        httpClient.execute(request);

    }

}
