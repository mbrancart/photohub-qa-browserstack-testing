package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import screens.ResultScreen;
import screens.searchComponents.PaginationFooter;
import screens.searchComponents.SearchHeader;
import webdriver.WebDriverManagerUtil;

public class VerifyPagination {
    WebDriver driver = WebDriverManagerUtil.getDriver();
    ResultScreen resultScreen = new ResultScreen(driver);
    private SearchHeader searchHeader;
    private PaginationFooter paginationFooter;


    @When("^I select the page size amount \"([^\"]*)\"$")
    public void iSelectThePageSizeAmount(String amountOfImagesPerPage) {
        searchHeader = new SearchHeader(driver);
        searchHeader.selectPageSize(amountOfImagesPerPage);

        if (!amountOfImagesPerPage.equals("60")) {
            this.paginationFooter = new PaginationFooter(driver);
        }
    }

    @Then("^I expect to get \"([^\"]*)\" results$")
    public void iExpectToGetResults(String amountOfExpectedResults) {
        int amountOfActualResults = resultScreen.getResultList().size();
        waitForPageToHaveCondition(webDriver -> amountOfActualResults == Integer.parseInt(amountOfExpectedResults));
        Assert.assertEquals(String.format("Selected page size and size of result list don't match, %s is not equal to %d",
                amountOfExpectedResults, amountOfActualResults),Integer.parseInt(amountOfExpectedResults), amountOfActualResults);
    }

    @Then("^I press the nextPage button$")
    public void iPressTheNextPageButton() {
        paginationFooter.goToNextPage();
    }

    @Then("^I go to the first page$")
    public void iGoToTheFirstPage() {
        paginationFooter.setIndex("1\n");
    }

    @Then("^I expect there is no previous page button$")
    public void iExpectThereIsNoPreviousPageButton() {
        PaginationFooter newPaginationFooter = new PaginationFooter(driver);
        Assert.assertTrue(newPaginationFooter.prevPageButton.getAttribute("class").contains("hidden"));
    }

    @Then("^I go to the last page$")
    public void iGoToTheLastPage() {
        paginationFooter.setIndex(paginationFooter.getMaxAmountOfPages() + "\n");
    }

    @Then("^I expect there is no next page button$")
    public void iExpectThereIsNoNextPageButton() {
        PaginationFooter newPaginationFooter = new PaginationFooter(driver);
        Assert.assertTrue(newPaginationFooter.nextPageButton.getAttribute("class").contains("hidden"));
    }

    @And("^I go to page number \"([^\"]*)\"$")
    public void iGoToPageNumber(int pageNumber) {
        paginationFooter.setIndex(pageNumber + "\n");
        paginationFooter.setInintialIndex(pageNumber);
    }

    @Then("^I expect to get the previous page of images$")
    public void iExpectToGetThePreviousPageOfImages() {
        PaginationFooter newPage = new PaginationFooter(driver);
        Assert.assertEquals("The new page does not have an index higher than the previous page",
                newPage.getIndex(), paginationFooter.getIndex() - 1);
    }

    @Then("^I expect to get the first page$")
    public void iExpectToGetTheFirstPage() {
        waitForPageToHaveCondition(webDriver -> new PaginationFooter(webDriver).getIndex() == 1);
        PaginationFooter newPage = new PaginationFooter(driver);
        Assert.assertEquals("The page was not redirected to the first page",
                1, newPage.getIndex());
    }

    @Then("^I expect to get a new page of images$")
    public void iExpectToGetANewPageOfImages() {
        waitForPageToHaveCondition(webDriver -> new PaginationFooter(webDriver).getIndex() == paginationFooter.getIndex() + 1);
        PaginationFooter newPage = new PaginationFooter(driver);
        Assert.assertTrue("The new page does not have an index higher than the previous page",
                newPage.getIndex() == paginationFooter.getIndex() + 1);
    }

    private void waitForPageToHaveCondition(ExpectedCondition<Boolean> condition) {
        try {
            new WebDriverWait(driver, 60).until(condition);
        } catch (Exception e) {
            new PaginationFooter(driver).takeScreenshot();
        }
    }
}
