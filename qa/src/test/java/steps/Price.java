package steps;

import java.util.Arrays;

public enum Price {
    ONE("1", "price-range-cheap","low"),
    TWO("2", "price-range-standard","normal"),
    THREE("3", "price-range-expensive","expensive"),
    NOTSET("Not set", "",""),
    FREE("Free","price-range-free","contract");

    private final String className;
    private final String name;
    private final String value;

    Price(String name, String className, String value) {
        this.name = name;
        this.className = className;
        this.value=value;
    }

    public static Price getPrice(String name) {
        return Arrays.asList(Price.values()).stream()
                .filter(price -> price.name.equalsIgnoreCase(name))
                .findAny()
                .orElseThrow(() -> new RuntimeException("Price not found! " + name));
    }

    public String getClassName() {
        return className;
    }

    public String getValue(){
        return value;
    }
}
