package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import screens.FullViewScreen;
import screens.ResultScreen;
import utils.MetadataUtils;
import webdriver.WebDriverManagerUtil;

public class VerifyFullView {

    private WebDriver driver = WebDriverManagerUtil.getDriver();
    FullViewScreen fullViewScreen = new FullViewScreen(driver);
    ResultScreen resultScreen = new ResultScreen(driver);

    @When("^I press the next button$")
    public void iPressTheNextButton() {
        fullViewScreen.clickNext();
    }

    @When("^I press the previous button$")
    public void iPressThePreviousButton() {
        fullViewScreen.clickPrevious();
    }

    @Then("^the image \"([^\"]*)\" is shown$")
    public void theNextImageIsShown(String nextImageNr) {
        String expected  = resultScreen.getImageElement(Integer.parseInt(nextImageNr)).getAttribute("src");
        Assert.assertEquals("image shown is not the next image", expected, fullViewScreen.getImageSource());
    }

    @When("^I select the next \"([^\"]*)\" images$")
    public void iSelectTheNextImages(int nrImages) throws InterruptedException {
       for(int i = 0; i < nrImages; i++) {
           fullViewScreen.clickNext();
           Thread.sleep(750);
           fullViewScreen.addPictureToSelection();
       }
    }
    
    @When("^I close the full view$")
    public void iCloseTheFullView()  {
        fullViewScreen.closeFullView();
    }

    @And("^I delete the displayed image from diocontent$")
    public void iDeleteTheDisplayedImageFromDiocontent() {
        MetadataUtils.deleteAsset( Integer.parseInt( fullViewScreen.getMetaInfoValue(driver.findElement(By.className("preview")),"ID") ) );
    }

    @And("^I select the previous \"([^\"]*)\" images$")
    public void iSelectThePreviousImages(int nrImages) {
        for(int i = 0; i < nrImages; i++) {
            fullViewScreen.clickPrevious();
            fullViewScreen.addPictureToSelection();
        }
    }
}
