package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import screens.SearchScreen;
import screens.TabBanner;
import screens.UploadScreen;
import webdriver.WebDriverManagerUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly;
import static java.util.concurrent.TimeUnit.SECONDS;

public class VerifyUpload {
    WebDriver driver = WebDriverManagerUtil.getDriver();
    TabBanner tabBanner = new TabBanner(driver);
    SearchScreen searchScreen = new SearchScreen(driver);
    UploadScreen uploadScreen=new UploadScreen(driver);

    private static final Log LOGGER = LogFactory.getLog(VerifyUpload.class);


    @When("^I open the upload tab$")
    public void iOpenTheUploadTab(){
        tabBanner.openUploadTab();
        Assert.assertTrue("upload tab is not opened",tabBanner.verifyUploadTabOpened());
        Assert.assertTrue(uploadScreen.verifyUploadZonePresent());
    }

    @And("^I upload \"([^\"]*)\"$")
    public void iUpload(String filepath){
        ((RemoteWebDriver)driver).setFileDetector(new LocalFileDetector());
        uploadScreen.uploadFiles(new String[]{filepath});
    }

    @And("^I upload \"([^\"]*)\" with shooting date \"([^\"]*)\"$")
    public void iUploadWithShootingDate(String filepath, String shootingDate){
        uploadScreen.uploadFilesWithShootingDate(new String[]{filepath}, shootingDate);
    }

    @And("^I upload additional files \"([^\"]*)\"$")
    public void iUploadAdditionalFiles(String filepath){
        uploadScreen.addExtraFiles(new String[]{filepath});
    }

    @And("^I drop \"([^\"]*)\"$")
    public void iDrop(String filepath){
        uploadScreen.dropFiles(new String[]{filepath});
    }

    @Then("^My file is visible in the list$")
    public void myFileIsVisibleInTheList() {
        Assert.assertTrue("not all images contain a placeholder image", uploadScreen.isPlaceholderDisplayedForAllImages());
        uploadScreen.waitForImagesToBeDisplayed();
        Assert.assertTrue("list of uploaded files is empty",uploadScreen.listUploadedPictures.size()!=0);
        Assert.assertEquals("Image count is not the expected number ",uploadScreen.getExpectedImagesOnPage(),uploadScreen.getImageCount());
     }

    @Then("^I get an error popup message$")
    public void iGetAnErrorPopupMessage()  {
        Assert.assertTrue(uploadScreen.verifyUploadErrorModalIsPresent());
    }

    @And("^I can fill in metadata and confirm upload$")
    public void iCanFillInMetadataAndConfirmUpload() {
        uploadScreen.fillInMetadataAndClickFinish("caption created on "+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime()),"picture squad","test automation");
        Assert.assertTrue("redirection to search tab doesn't work",tabBanner.verifySearchTabOpened());
    }

    @And("^I can add brands \"([^\"]*)\"$")
    public void iCanAddBrands(List<String> brands) {
        uploadScreen.addBrands(brands);
        LOGGER.debug("Added brands: " + brands);
    }

    @And("^I can fill in a big caption and confirm upload$")
    public void iCanFillInABigCaptionAndConfirmUpload() {
        String caption = RandomStringUtils.randomAlphabetic(UploadScreen.CAPTION_SIZE_LIMIT + 10);
        final String credit = "test automation";

        //Assert.assertFalse(uploadScreen.verifyCaptionToggleVisible(1));
        uploadScreen.fillInMetaData(caption,"picture squad", credit);
        Assert.assertTrue(uploadScreen.verifyCaptionToggleVisible(1));

        uploadScreen.clickFinishMetadata(caption, credit);
        Assert.assertTrue("redirection to search tab doesn't work",tabBanner.verifySearchTabOpened());
    }


    @And("^Uploaded image is visible on search page$")
    public void uploadedImageIsVisibleOnSearchPage() throws InterruptedException {
        Thread.sleep(1000);
        uploadScreen.verifyUploadPresentInResultScreen();
    }

    @And("^Uploaded image is visible on search page for brands \"([^\"]*)\"$")
    public void uploadedImageIsVisibleOnSearchPageForBrands(List<String> brands) throws InterruptedException {
        Thread.sleep(1000);
        brands.forEach(this::validateImageIsVisibleForBrand);
    }

    @And("^Uploaded image is not visible on search page for brands \"([^\"]*)\"$")
    public void uploadedImageIsNotVisibleOnSearchPageForBrands(List<String> brands) throws InterruptedException {
        Thread.sleep(1000);
        brands.forEach(this::validateImageIsNotVisibleForBrand);

    }

    private void validateImageIsVisibleForBrand(String brand) {
        LOGGER.info("Validate image is visible for brand: " + brand);
        final boolean doNotRefreshResultScreen = false;
        sleepUninterruptibly(2, SECONDS);
        searchScreen.selectBrand(brand);
        searchScreen.clickSearch();
        sleepUninterruptibly(2, SECONDS);
        uploadScreen.verifyUploadPresentInResultScreen(doNotRefreshResultScreen);
    }

    private void validateImageIsNotVisibleForBrand(String brand) {
        LOGGER.info("Validate image is not visible for brand: " + brand);
        final boolean doNotRefreshResultScreen = false;
        sleepUninterruptibly(2, SECONDS);
        searchScreen.selectBrand(brand);
        searchScreen.clickSearch();
        sleepUninterruptibly(2, SECONDS);
        uploadScreen.verifyUploadNotPresentInResultScreen(doNotRefreshResultScreen);
    }

    @And("^I upload \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iUploadAnd(String firstFile, String secondFile) {
        uploadScreen.uploadFiles(new String[]{firstFile,secondFile});
    }

    @And("^Error is displayed when caption and credits are empty$")
    public void errorIsDisplayedWhenCaptionAndCreditsAreEmpty(){
        uploadScreen.fillInMetadataAndClickFinish("   ","picture squad","   ");
        Assert.assertTrue(uploadScreen.isSubmitButtonDisabled());
    }

    @And("^I press submit without file chosen$")
    public void iPressSubmitWithoutFileChosen() {
        uploadScreen.clickSubmit();
    }


    @Then("^The upload page is still displayed$")
    public void theUploadPageIsStillDisplayed(){
        Assert.assertTrue("submit button is not displayed anymore",uploadScreen.submitButton.isDisplayed());
    }

    @And("^I expect to see caption \"([^\"]*)\" photographer \"([^\"]*)\" and credit \"([^\"]*)\"$")
    public void iExpectToSeeCaptionPhotographerAndCredit(String expectedCaption, String expectedPhotographer, String expectedCredit) {
        Assert.assertTrue("list of uploaded files is empty",uploadScreen.listUploadedPictures.size()!=0);
        Assert.assertEquals("Image count is not the expected number ",uploadScreen.getExpectedImagesOnPage(),uploadScreen.getImageCount());
        Assert.assertEquals("caption is not the expected one",expectedCaption,uploadScreen.getCaptionOfElement(0));
        Assert.assertEquals("photographer is not the expected one",expectedPhotographer,uploadScreen.getPhotographerOfElement(0));
        Assert.assertEquals("credit is not the expected one",expectedCredit,uploadScreen.getCreditsOfElement(0));
    }

    @And("^I can fill in metadata with caption \"([^\"]*)\"$")
    public void iCanFillInMetadataWithCaption(String caption) {
        uploadScreen.setCaptionToSearch(caption+" "+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime()));
        uploadScreen.fillInMetadataAndClickFinish(uploadScreen.getCaptionToSearch(),"picture squad","test automation");
        Assert.assertTrue("redirection to search tab doesn't work",tabBanner.verifySearchTabOpened());
    }

    @And("^I can delete the last image$")
    public void iCanDeleteTheLastImage() {
        Assert.assertTrue(uploadScreen.deleteLastImage());
    }

    @And("^Displayed caption is \"([^\"]*)\"$")
    public void displayedCaptionIs(String expectedCaption){
        Assert.assertEquals("caption is not the expected one",expectedCaption,uploadScreen.getDisplayedCaptionOfPreview(1));
        uploadScreen.fillInMetadataAndClickFinish("","test photographer","test Credits");
        Assert.assertEquals("caption is not the expected one",expectedCaption,uploadScreen.getDisplayedCaptionOfPreview(1));
    }

    @And("^The submit button is disabled$")
    public void theSubmitButtonIsDisabled() {
        Assert.assertTrue("Submit button is not disabled",!uploadScreen.finishButton.isEnabled());
    }

    @And("^The upload zone is available$")
    public void theUploadZoneIsAvailable() {
        Assert.assertTrue("The upload zone is not available",uploadScreen.verifyUploadZonePresent());
    }

    @And("^I upload another image \"([^\"]*)\"$")
    public void iUploadAnotherImage(String imagePath){
        uploadScreen.uploadAdditionalFiles(imagePath);
    }

    @Then("^I can see \"([^\"]*)\" of images uploaded$")
    public void iCanSeeOfImagesUploaded(String amount) {
        Assert.assertEquals(Integer.parseInt(amount), uploadScreen.getImageCount());
    }

    @And("^I wait for my file to become visible in the list$")
    public void iWaitForMyFileToBecomeVisibleInTheList() {
        new WebDriverWait(driver, 60).until((ExpectedCondition<Boolean>) webDriver ->
                uploadScreen.listUploadedPictures.size() == uploadScreen.getExpectedImagesOnPage()
        );
    }

    @And("^I delete the last image$")
    public void iDeleteTheLastImage() {
        uploadScreen.deleteLastImage();
    }

    @And("^I can confirm my upload$")
    public void iCanConfirmMyUpload() {
        uploadScreen.clickFinish();
    }

    @Then("^I can fill in metadata$")
    public void iCanFillInMetadata() {
        new WebDriverWait(driver, 60).until((ExpectedCondition<Boolean>) webDriver ->
                webDriver.findElement(By.cssSelector("button.button.button--secondary")).isDisplayed()
        );
        uploadScreen.fillInMetaDataForAllImages("caption created on "+new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime()),"picture squad","test automation");
    }

    @And("^I delete the first image$")
    public void iDeleteTheFirstImage() {
        uploadScreen.deleteImageWithIndex(0);
    }

    @And("^I select all images$")
    public void iSelectAllImages() {
        uploadScreen.clickSelectAllImages();
    }
}
