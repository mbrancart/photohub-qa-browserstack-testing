package steps;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import screens.ResultScreen;
import screens.SearchScreen;
import utils.MetadataUtils;
import webdriver.WebDriverManagerUtil;

import java.util.ArrayList;
import java.util.List;

public class VerifyBrandFilter {

    private WebDriver driver = WebDriverManagerUtil.getDriver();
    private SearchScreen searchScreen= new SearchScreen(driver);
    private ResultScreen resultScreen= new ResultScreen(driver);
    private static final Log LOGGER = LogFactory.getLog(VerifyBrandFilter.class);


    @When("^I select the brand \"([^\"]*)\"$")
    public void iSelectTheBrand(String brand) {
       searchScreen.selectBrand(brand);
        LOGGER.info("The selected brand is: " + brand);
    }

    @When("^I select all brands$")
    public void iSelectAllBrands() {
        searchScreen.selectBrand("All Brands");
    }

    @And("^Brand filter is still \"([^\"]*)\"$")
    public void brandFilterIsStill(String brand){
        Assert.assertEquals("Selected brand filter is not equals to "+brand,brand,searchScreen.getSelectedBrand());
    }

    @And("^Displayed images are images from \"([^\"]*)\"$")
    public void displayedImagesAreImagesFrom(List<String> mediapools){
        if(!mediapools.get(0).equalsIgnoreCase("all")) {
            List<WebElement> resultList= resultScreen.getResultList();
            resultList.forEach(webElement -> verifyMediapool(mediapools, webElement));
        }

    }

    private void verifyMediapool(List<String> mediapools, WebElement webElement) {
        String imageId = resultScreen.getIdOfElement(webElement);
        String message = "image with id " + imageId + " is not part of the mediapools " + mediapools;
        Assert.assertTrue(message,resultScreen.verifyMediaPoolofImage(Integer.parseInt(imageId),mediapools));
    }

    @And("^the brands \"([^\"]*)\" are available in the dropdown$")
    public void theBrandsAreAvailableInTheDropdown(List<String> listOfAvailableBrands) {
        Assert.assertTrue("The Brand dropdown is not containing the expected brands",searchScreen.isBrandDropdownContainingBrands(listOfAvailableBrands));
    }
}
