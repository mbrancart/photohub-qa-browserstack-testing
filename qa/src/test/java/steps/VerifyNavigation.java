package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import screens.TabBanner;
import webdriver.WebDriverManagerUtil;

import java.net.MalformedURLException;
import java.util.List;

public class VerifyNavigation {
    WebDriver driver = WebDriverManagerUtil.getDriver();
    private TabBanner tabBanner=new TabBanner(driver);


    @And("^I open the search tab$")
    public void iOpenTheSearchTab() {
        tabBanner.openSearchTab();
    }

    @And("^I open the bookmarks tab$")
    public void iOpenTheBookmarksTab() {
        tabBanner.openBookmarksTab();
        Assert.assertTrue("Bookmarks tab is not open.",tabBanner.verifyBookmarksTabOpened());
    }

    @Then("^The brand is still \"([^\"]*)\"$")
    public void theBrandIsStill(String brand) throws MalformedURLException {
        List<String> queryParameters = tabBanner.getQueryParameters();
        Assert.assertTrue("query doesn't contain context brand. query="+ queryParameters, queryParameters.contains("brand="+brand));
    }

    @And("^The redirect is still \"([^\"]*)\"$")
    public void theRedirectIsStill(String redirect) throws MalformedURLException {
        List<String> queryParameters = tabBanner.getQueryParameters();
        Assert.assertTrue("query doesn't contain redirect. query="+ queryParameters, queryParameters.contains("redirect="+redirect));
    }
}
