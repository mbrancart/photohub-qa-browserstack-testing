package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import screens.FullViewScreen;
import screens.ResultScreen;
import webdriver.WebDriverManagerUtil;

import java.time.LocalDate;
import java.util.List;

public class VerifySorting {

    WebDriver driver= WebDriverManagerUtil.getDriver();
    ResultScreen resultScreen=new ResultScreen(driver);
    FullViewScreen fullViewScreen=new FullViewScreen(driver);
    List<WebElement> resultList=resultScreen.getResultList();

    private static final Log LOGGER = LogFactory.getLog(VerifySorting.class);

    @Then("^Results are ordered by \"([^\"]*)\"$")
    public void resultsAreOrderedBy(String sortingOrder){
        String currentId,actualDate,previousId="";
        LocalDate previousDate=null;
        int cpt=0;

        for (WebElement currentElement:resultList){
            //open metadata and gather info
            resultScreen.openFullView(currentElement);
            currentId = fullViewScreen.getMetaInfoValue(currentElement,"ID");
            if(sortingOrder.contains("import"))
                actualDate=fullViewScreen.getMetaInfoValue(currentElement,"Import date");
            else
                actualDate=fullViewScreen.getMetaInfoValue(currentElement,"Shooting date");
            if(actualDate.isEmpty()) {
                System.out.println("stopping tests, shooting date is empty");
                break;
            }
            LocalDate currentDate = LocalDate.parse(actualDate);

            //no need to compare wfirst element with previous one
            if(cpt!=0) {
                //verify if date of current element is greater (or equals) than previous date
                LOGGER.debug(String.format("Comparing pic id:%s date :%s  with other pic id:%s date:%s",currentId,currentDate.toString(),previousId,previousDate.toString()));

                if(sortingOrder.contains("Most recent"))
                    Assert.assertTrue(currentId +" date is before previous date of id " +previousId,previousDate.compareTo(currentDate)>=0);
                else
                    Assert.assertTrue(currentId +" date is after previous date of id " +previousId,previousDate.compareTo(currentDate)<=0);
            }

            //close metadata
            resultScreen.closeFullView();
            previousDate=currentDate;
            previousId=currentId;
            cpt++;
        }

    }

    @And("^I order results by \"([^\"]*)\"$")
    public void iOrderResultsBy(String orderBy) {
        resultScreen.selectSortingOrder(orderBy);
        Assert.assertTrue("Sorting filter is not :"+orderBy,resultScreen.verifySortingOrderIsUnchanged(orderBy));
    }
}
