package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import screens.BookmarksScreen;
import webdriver.WebDriverManagerUtil;

public class VerifyBookmarks {

    private WebDriver driver = WebDriverManagerUtil.getDriver();
    private BookmarksScreen bookmarksScreen = new BookmarksScreen(driver);

    @And("^I open the folder named \"([^\"]*)\"$")
    public void iOpenTheFolderNamed(String bookmarkName) {
        bookmarksScreen.openFolder(bookmarkName);
        Assert.assertTrue("The bookmark name is not in bold",bookmarksScreen.isFolderNameInBold(bookmarkName));
        //will be true when getting a response from the backend
        Assert.assertTrue("the page title doesn't contain the bookmarkName: "+bookmarkName,bookmarksScreen.isTitleContainingFolderName(bookmarkName));
    }

    @And("^I click on the arrow next to folder \"([^\"]*)\"$")
    public void iClickOnTheArrowNextToFolder(String folderName){
        bookmarksScreen.clickOnArrowBasedOnFolderName(folderName);
    }

    @Then("^The subfolders of \"([^\"]*)\" are visible$")
    public void theSubFoldersOfAreVisible(String bookmarkName) {
        Assert.assertTrue(bookmarksScreen.areSubfoldersVisible(bookmarkName));
    }

    @And("^I press the next bookmarks page button$")
    public void iPressTheNextBookmarksPageButton() {
        bookmarksScreen.clickOnNextPage();
    }

    @Then("^I expect to get the next page of bookmarks$")
    public void iExpectToGetTheNextPageOfBookmarks() {
        Assert.assertEquals(bookmarksScreen.getExpectedBookmarksOnPage(),bookmarksScreen.getActualNumberBookmarksOnPage());
    }
}
