package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.persgroep.diocontent.images.api.asset.Asset;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import screens.FullViewScreen;
import screens.ResultScreen;
import utils.MetadataUtils;
import webdriver.WebDriverManagerUtil;

import java.util.*;

import static utils.MetadataUtils.*;

public class VerifyImageSelection {

    WebDriver driver = WebDriverManagerUtil.getDriver();
    ResultScreen resultScreen=new ResultScreen(driver);
    FullViewScreen fullViewScreen=new FullViewScreen(driver);
    List<WebElement> resultList=resultScreen.getResultList();
    List<WebElement> selectedItems= new ArrayList<>();
    Actions actions=new Actions(driver);
    WebElement randomElement;
    int expectedImagesOnBanner,expectedExtraNumberBanner=0;

    @When("^I \"([^\"]*)\" a random image$")
    public void iARandomImage(String action){

        if(action.equals("select")){
            //generating random nr. and hovering it
            int random = (int)(Math.random() * resultList.size()-1 );
            randomElement=resultList.get(random);
            actions.moveToElement(randomElement).build().perform();
            randomElement.findElement(By.className("search-result__toggle--add")).click();
            expectedImagesOnBanner++;
            }
        else {
            randomElement.findElement(By.className("search-result__toggle--remove")).click();
            expectedImagesOnBanner--;
           }
    }

    @Then("^The image is not selected anymore$")
    public void theImageIsNotSelectedAnymore(){
        Assert.assertFalse("selection banner is displayed",resultScreen.selectionBanner.isDisplayed());
        Assert.assertFalse("Element has a yellow border", resultScreen.checkElementHasYellowBorder(randomElement));
    }

    @Then("^The images are selected$")
    public void theImagesAreSelected(){
        for (WebElement element:selectedItems)
            Assert.assertTrue("Element has no yellow border", resultScreen.checkElementHasYellowBorder(element));
    }

    @And("^I can see my selection on the bottom$")
    public void iCanSeeMySelectionOnTheBottom(){
        verifyImagesOnBanner(expectedImagesOnBanner, expectedExtraNumberBanner);
    }

    @Then("^I can see my selection of \"([^\"]*)\" images on the bottom$")
    public void iCanSeeMySelectionOfImagesOnTheBottom(int nrImages)  {
        verifyImagesOnBanner(nrImages);
    }

    private void verifyImagesOnBanner(int numberOfSelectedImages) {
        int expectedExtraNumberOnBanner=0;
        if(numberOfSelectedImages>5) {
            expectedExtraNumberOnBanner=numberOfSelectedImages-4;
        }
        verifyImagesOnBanner(numberOfSelectedImages, expectedExtraNumberOnBanner);
    }
    private void verifyImagesOnBanner(int expectedImagesOnBanner, int expectedExtraNumberOnBanner) {
        if(expectedImagesOnBanner!=0){
            Assert.assertTrue("selection banner is not displayed",resultScreen.selectionBanner.isDisplayed());
            Assert.assertEquals("number of selected images is not correct",expectedImagesOnBanner,driver.findElements(By.className("banner__preview")).size());
        }
        else
            Assert.assertFalse("selection banner is displayed",resultScreen.selectionBanner.isDisplayed());
        if(expectedExtraNumberOnBanner!=0)
            Assert.assertEquals("number of images not visible on banner is not correct",expectedExtraNumberOnBanner,resultScreen.getNumberOfExtraSelectedImages());
    }

    @Then("^I select \"([^\"]*)\" images by using \"([^\"]*)\"$")
    public void iSelectImagesByUsing(String numberOfImages, String action) {
        expectedImagesOnBanner =Integer.parseInt(numberOfImages);
        if(expectedImagesOnBanner>5)
            expectedExtraNumberBanner=expectedImagesOnBanner-4;
        int randomNumber;
        List<Integer> randomNumbersList = new ArrayList<>();
        for (int i = 0; i < expectedImagesOnBanner; i++) {
            randomNumber = (int) (Math.random() * resultList.size() - 1);
            //Ensure that same item won't be selected twice
            while (randomNumbersList.contains(randomNumber))
                randomNumber = (int) (Math.random() * resultList.size() - 1);
            System.out.println("selecting image number:" + randomNumber);
            randomElement = resultList.get(randomNumber);
            resultScreen.selectElement(action, randomElement);
            //selection seems to randomly fail
            if (!randomElement.getCssValue("outline").equalsIgnoreCase("rgb(255, 194, 15) solid 3px")){
                System.out.println("retrying to select the element");
                resultScreen.selectElement(action, randomElement);
            }
            selectedItems.add(randomElement);
            randomNumbersList.add(randomNumber);
        }
    }


    @When("^I open the full view of an image$")
    public void iOpenTheFullViewOfAnImage() {
        int randomNumber;
        //this will only work when continuous full view pagination will be implemented
        randomNumber = (int) (Math.random() * resultList.size() - 1);
        openFullViewImage(randomNumber);
    }

    @When("^I open the full view of an image \"([^\"]*)\"$")
    public void iOpenTheFullViewOfAnImageWithNr(String imageNr) {
        openFullViewImage(Integer.parseInt(imageNr));
    }

    private void openFullViewImage(int imageNr) {
        randomElement = resultList.get(imageNr);
        resultScreen.openFullView(randomElement);
    }

    @Then("^Information of the image is displayed$")
    public void informationOfTheImageIsDisplayed() {
        WebElement currentImage=driver.findElement(By.className("preview"));
        String imageId=fullViewScreen.getMetaInfoValue(currentImage,"ID");
        Asset asset = MetadataUtils.getAsset(Integer.parseInt(imageId));
        String caption= fullViewScreen.caption.getText().replaceAll("\\s+","").replaceAll("\\u00a0","");
        Assert.assertTrue("full view details are not displayed",currentImage.findElement(By.className("preview__detail")).isDisplayed());

        Assert.assertEquals("Image with id "+imageId+" has wrong Caption",getCaption(asset),caption);
        Assert.assertEquals("Image with id "+imageId+" has wrong Credits value", getCredits(asset),fullViewScreen.getMetaInfoValue(currentImage,"Credits"));
        Assert.assertEquals("Image with id "+imageId+" has wrong photographer", getPhotographer(asset),fullViewScreen.getMetaInfoValue(currentImage,"Photographer"));
        Assert.assertEquals("Image with id "+imageId+" has wrong import date", getCreateDate(asset),fullViewScreen.getStringFromDate(fullViewScreen.getMetaInfoValue(currentImage,"Import date")));
        Assert.assertEquals("Image with id "+imageId+" has wrong comes from", getComesFrom(asset),fullViewScreen.getMetaInfoValue(currentImage,"Comes from"));
        Assert.assertEquals("Image with id "+imageId+" has wrong Shooting date", getShootingDate(asset),fullViewScreen.getStringFromDate(fullViewScreen.getMetaInfoValue(currentImage,"Shooting date")));
        Assert.assertEquals("Image with id "+imageId+" has wrong Location", getLocation(asset),fullViewScreen.getMetaInfoValue(currentImage,"Location"));

    }


    @And("^I can delete an image from the selection$")
    public void iCanDeleteAnImageFromTheSelection() {
        resultScreen.removeFirstElementFromSelection();
        expectedImagesOnBanner--;
        expectedExtraNumberBanner--;

        if (expectedImagesOnBanner > 0) {
            Assert.assertTrue("selection banner is not displayed", resultScreen.selectionBanner.isDisplayed());
            Assert.assertEquals("number of selected images is not correct", expectedImagesOnBanner, driver.findElements(By.className("banner__preview")).size());
        } else
            Assert.assertFalse("selection banner is displayed", resultScreen.selectionBanner.isDisplayed());
        if (expectedExtraNumberBanner > 0)
            Assert.assertEquals("number of images not visible on banner is not correct", expectedExtraNumberBanner, resultScreen.getNumberOfExtraSelectedImages());
    }

    @And("^I remove the first image from selection$")
    public void iRemoveTheFirstImageFromSelection() {
        resultScreen.removeFirstElementFromSelection();
    }

    @Then("^I can see my selection on the bottom and the \"([^\"]*)\" remain the same$")
    public void iCanSeeMySelectionOnTheBottomAndTheRemainTheSame(String amountOfSelectedImages) throws Throwable {
        Assert.assertTrue(resultScreen.selectionBanner.isDisplayed());
        List<WebElement> bannerElements = driver.findElements(By.className("banner__preview"));
        Assert.assertEquals(Integer.parseInt(amountOfSelectedImages), bannerElements.size());
    }

    @When("^I add the current image to selection$")
    public void iAddTheCurrentImageToSelection() {
        fullViewScreen.addPictureToSelection();
    }

    @And("^I remove the current image from selection$")
    public void iRemoveTheCurrentImageFromSelection() {
        fullViewScreen.removePictureToSelection();
    }
}
