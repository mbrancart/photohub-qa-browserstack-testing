package steps;

import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import screens.HeaderScreen;
import webdriver.WebDriverManagerUtil;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

public class VerifySourceFiles {

    private final int MD5_LENGTH = 32;

    private WebDriver driver = WebDriverManagerUtil.getDriver();
    private HeaderScreen headerScreen = new HeaderScreen(driver);

    @Then("^I expect the correct version of the javascript file$")
    public void iExpectTheCorrectVersionOfTheJavascriptFile() {
        String expectedMD5 = getJavaScriptVersionFromUrl();
        String resultMD5 = calculateMD5();

        Assert.assertEquals("The MD5 of the downloaded javascript file does not match the version in the url",
                expectedMD5,
                resultMD5);
    }

    @Then("^I expect the correct version of the css file$")
    public void i_expect_the_correct_version_of_the_css_file() {
        String expectedMD5 = getMD5FromUrl(headerScreen.getCssUrl());
        String resultMD5 = calculateMD5Css();

        Assert.assertEquals("The MD5 of the downloaded css file does not match the version in the url",
                expectedMD5,
                resultMD5);

    }

    private String getJavaScriptVersionFromUrl() {
        String javaScriptUrl = headerScreen.getJavaScriptUrl();
        return getMD5FromUrl(javaScriptUrl);
    }

    private String getMD5FromUrl(String url) {
        return url.substring(url.length() - MD5_LENGTH);
    }

    private String calculateMD5() {
        String javascriptContent = headerScreen.getJavaScriptContent() + "\n";
        return md5Hex(javascriptContent);
    }

    private String calculateMD5Css() {
        String cssContent = headerScreen.getCssContent() + "\n";
        return md5Hex(cssContent);
    }
}
