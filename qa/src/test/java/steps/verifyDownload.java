package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import screens.FullViewScreen;
import screens.ResultScreen;
import utils.DownloadOriginal;
import utils.Metadata;
import webdriver.WebDriverManagerUtil;

import java.util.ArrayList;
import java.util.List;

public class verifyDownload {

    private WebDriver driver = WebDriverManagerUtil.getDriver();
    private FullViewScreen fullViewScreen=new FullViewScreen(driver);
    private ResultScreen resultScreen = new ResultScreen(driver);
    private DownloadOriginal downloadOriginal= new DownloadOriginal();
    private List<utils.Metadata> listMetaFrontend = new ArrayList<>();

    @And("^I download the original of the current image$")
    public void iDownloadTheOriginalOfTheCurrentImage() {
        fullViewScreen.clickDownloadImage();
    }

    @Then("^I can see the metadata of the image$")
    public void iCanSeeTheMetadataOfTheImage() {
        WebElement currentImage=driver.findElement(By.className("preview"));
        Metadata photohubMetadata = new Metadata(fullViewScreen.getCaption(),fullViewScreen.getMetaInfoValue(currentImage,"Credits"));
        Metadata imageMetadata= downloadOriginal.readSingleImageMetadata();
        Assert.assertTrue(photohubMetadata.equals(imageMetadata));
    }

    @And("^I download a set of images$")
    public void iDownloadASetOfImages() {
        resultScreen.clickDownloadImages();
    }

    @Then("^I can see the metadata of all images$")
    public void iCanSeeTheMetadataOfAllImages() {
        List<Metadata> imagesMetadata = downloadOriginal.readZipMetadata();
        Assert.assertTrue("the downloaded images metadata are not the same as the displayed ones",listMetaFrontend.equals(imagesMetadata));
    }

    @And("^I select and store meta info of the next \"([^\"]*)\" image$")
    public void iSelectAndGetMetaInfoOfTheNextImage(int numberOfImages){
        listMetaFrontend= fullViewScreen.selectImagesAndGetMetaInfo(numberOfImages);
        Assert.assertFalse("List of metadata in the frontend is empty",listMetaFrontend.isEmpty());
    }

    @Then("^I get a zip of \"([^\"]*)\" images$")
    public void iGetAZipOfImages(int expectedNumberOfZippedImages){
        List<Metadata> imagesMetadata = downloadOriginal.readZipMetadata();
        Assert.assertEquals("number of images in the zip is incorrect",expectedNumberOfZippedImages,imagesMetadata.size());
    }
}
