package steps;

import static org.junit.jupiter.api.Assertions.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import screens.ResultScreen;
import webdriver.WebDriverManagerUtil;

public class VerifyStandalone {
    WebDriver driver = WebDriverManagerUtil.getDriver();
    ResultScreen resultScreen = new ResultScreen(driver);
    private static final Log LOGGER = LogFactory.getLog(VerifyBrandFilter.class);

    @Given("^I am on the standalone home page$")
    public void iAmOnTheStandaloneHomePage() {
        String standaloneLink = getStandaloneLink();
        LOGGER.info("navigating to standalone: " + standaloneLink);
        driver.get(standaloneLink);
        resultScreen.waitForList(standaloneLink);
    }

    @Then("^the add images to article button is not visible$")
    public void theAddImagesToArticleButtonIsNotVisible() {
        assertThrows(NoSuchElementException.class, () -> {
            driver.findElement(By.className("banner__add-to-article"));
        });
    }

    private String getStandaloneLink() {
        String standaloneLink = getSearchStandaloneLink();
        if (standaloneLink.contains("localhost")) {
            return adaptForLocalhost(standaloneLink);
        }
        return standaloneLink;
    }

    private String getSearchStandaloneLink() {
        return WebDriverManagerUtil.getStandaloneLink() + "search";
    }

    private String adaptForLocalhost(String standaloneLink) {
        return standaloneLink + "?standalone=true";
    }
}
