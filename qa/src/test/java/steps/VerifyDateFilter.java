package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import datefilters.Filter;
import datefilters.PeriodSpan;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import screens.FullViewScreen;
import screens.ResultScreen;
import screens.SearchScreen;
import utils.BrusselsDateTimeToSystemTimeZoneConverter;
import webdriver.WebDriverManagerUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class VerifyDateFilter {

    private String defaultPeriod = "All";
    private WebDriver driver = WebDriverManagerUtil.getDriver();
    private SearchScreen searchScreen = new SearchScreen(driver);
    private ResultScreen resultScreen;
    private FullViewScreen fullViewScreen;
    private static final Log LOGGER = LogFactory.getLog(VerifyDateFilter.class);

    public void initializeResultscreen() {
        resultScreen = new ResultScreen(driver);
    }

    public void initializeFullViewscreen() {
        fullViewScreen = new FullViewScreen(driver);
    }


    @Then("^I expect to see pictures with shooting date equals \"([^\"]*)\"$")
    public void iExpectToSeePicturesWithShootingDateEquals(String period) {
        verifyDateSpan(period, Filter.SHOOTINGDATE);
    }

    @And("^I expect to see pictures with importDate equals \"([^\"]*)\"$")
    public void iExpectToSeePicturesWithImportDateEquals(String period) {
        verifyDateSpan(period, Filter.IMPORTDATE);
    }

    public void verifyDateSpan(String period, Filter filter) {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 0)");
        PeriodSpan periodSpan = PeriodSpan.toTimeSpanOrDefault(period, PeriodSpan.ALL);
        initializeResultscreen();
        initializeFullViewscreen();

        System.out.println("size of the result list:"+resultScreen.getResultList().size());
        resultScreen.getResultList().forEach(webElement -> {
            resultScreen.openFullView(webElement);
            if (periodSpan != PeriodSpan.ALL)
                assertDisplayedDateInPeriodSpan(periodSpan, driver.findElement(By.className("preview")), filter);

            resultScreen.closeFullView();
        });
    }

    public void assertDisplayedDateInPeriodSpan(PeriodSpan periodSpan, WebElement webElement, Filter filter) {
        String displayedValue=fullViewScreen.getMetaInfoValue(webElement, filter.getFilterName());
        final String imageId = fullViewScreen.getMetaInfoValue(webElement, "ID");
        System.out.println("parsing date of picture id "+ imageId);
        LocalDate displayedDate;
        if(filter == Filter.IMPORTDATE) {
            displayedDate = BrusselsDateTimeToSystemTimeZoneConverter.convert(displayedValue);
        } else {
            final String displayedDateText = displayedValue.substring(0, 10);
            displayedDate = BrusselsDateTimeToSystemTimeZoneConverter.convert(displayedDateText + "00:00");
        }
        Assert.assertTrue(String.format("picture with id %s has wrong %s : expected period between %s and %s but displays %s", imageId,filter.getFilterName(),periodSpan.from(),periodSpan.to(), displayedDate),periodSpan.from().compareTo(displayedDate) * displayedDate.compareTo(periodSpan.to()) >= 0);
    }

    @And("^The \"([^\"]*)\" date filter is still \"([^\"]*)\"$")
    public void theDateFilterIsStill(String filter, String expectedPeriod) {
        if (filter.contains("import"))
            Assert.assertEquals("Import date filter is not correct", expectedPeriod, searchScreen.getSelectedImportDate());
        else
            Assert.assertEquals("Shooting date filter is not correct", expectedPeriod, searchScreen.getSelectedShootingDate());
    }

    @When("^I select a shooting date period of \"([^\"]*)\"$")
    public void iSelectAShootingDatePeriodOf(String period) {
        String periodOrDefault = getPeriodOrDefault(period);
        searchScreen.selectShootingDate(periodOrDefault);
        LOGGER.info("Selected shooting date is: " + periodOrDefault);
    }

    @And("^I select a import date period of \"([^\"]*)\"$")
    public void iSelectAImportDatePeriodOf(String period) {
        String periodOrDefault = getPeriodOrDefault(period);
        searchScreen.selectImportDate(periodOrDefault);
        LOGGER.info("Selected import date is: " + periodOrDefault);
    }

    private String getPeriodOrDefault(String period) {
        return Optional.ofNullable(period).orElse(defaultPeriod);
    }

    @And("^I select an import date range from \"([^\"]*)\" to \"([^\"]*)\" in the text input$")
    public void iSelectAnImportDateRangeFromToInTheTextInput(String startDate, String endDate) {
        searchScreen.selectImportPeriodInputText(startDate,endDate);
    }

    @Then("^The \"([^\"]*)\" date range filter is still correct$")
    public void theDateRangeFilterIsStillCorrect(String input) {
        if(input.contains("import"))
            Assert.assertEquals("displayed date is not the expected one",searchScreen.getExpectedImportDateRange(),searchScreen.getActualImportDateRange());
        else
            Assert.assertEquals("displayed date is not the expected one",searchScreen.getExpectedShootingDateRange(),searchScreen.getActualShootingDateRange());

    }

    @And("^I select an import date range from \"([^\"]*)\" to \"([^\"]*)\" in the calendar$")
    public void iSelectAnImportDateRangeFromToInTheCalendar(String startDate, String endDate) {
        searchScreen.selectImportPeriodCalendar(startDate,endDate);

    }

    public void verifyBetweenDates(String startDate,String endDate){
        initializeResultscreen();
        initializeFullViewscreen();
        resultScreen.getResultList().forEach(webElement -> {
            resultScreen.openFullView(webElement);
            Date  d = null;
            try {
                d = new SimpleDateFormat("yyyy-MM-dd").parse(fullViewScreen.getMetaInfoValue(webElement, "Import cate"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Assert.assertTrue("image with id "+fullViewScreen.getMetaInfoValue(webElement,"ID")+"is not between selected range", fullViewScreen.getDateFromString(startDate).compareTo(d) * d.compareTo(fullViewScreen.getDateFromString(endDate)) >= 0);


            resultScreen.closeFullView();
        });
    }

    @And("^The \"([^\"]*)\" of the picture is between \"([^\"]*)\" and \"([^\"]*)\"$")
    public void theOfThePictureIsBetweenAnd(String field, String startDate, String endDate) {
        initializeResultscreen();
        initializeFullViewscreen();
        resultScreen.getResultList().forEach(webElement -> {
            resultScreen.openFullView(webElement);
            final String localDateText = fullViewScreen.getMetaInfoValue(driver.findElement(By.className("preview")), field);
            LocalDate displayedDate = fullViewScreen.getLocalDateFromString(localDateText);
            if(Filter.getFilter(field) == Filter.IMPORTDATE) {
                displayedDate = BrusselsDateTimeToSystemTimeZoneConverter.convert(localDateText);
            }
            Assert.assertTrue("image with id " + fullViewScreen.getMetaInfoValue(driver.findElement(By.className("preview")), "ID") + " is not between selected range " + startDate + " - " + endDate, fullViewScreen.getLocalDateFromString(startDate).compareTo(displayedDate) * displayedDate.compareTo(fullViewScreen.getLocalDateFromString(endDate)) >= 0);
            resultScreen.closeFullView();
        });
    }
}
