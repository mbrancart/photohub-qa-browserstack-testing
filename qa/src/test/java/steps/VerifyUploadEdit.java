package steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import screens.UploadScreen;
import webdriver.WebDriverManagerUtil;

import java.util.List;

public class VerifyUploadEdit {
    WebDriver driver = WebDriverManagerUtil.getDriver();
    UploadScreen uploadScreen=new UploadScreen(driver);

    @And("^I enter a \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iEnterAAnd(String arg0, String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^Both files have the same caption and credit$")
    public void bothFilesHaveTheSameCaptionAndCredit() {
        Assert.assertEquals(uploadScreen.getCaptionOfElement(0), uploadScreen.getCaptionOfElement(1));
        Assert.assertEquals(uploadScreen.getCreditsOfElement(0), uploadScreen.getCreditsOfElement(1));
        Assert.assertEquals(uploadScreen.getPhotographerOfElement(0), uploadScreen.getPhotographerOfElement(1));
    }

    @And("^I deselect all images$")
    public void iDeselectAllImages() {
        uploadScreen.clickSelectAllImages();
    }

    @Then("^I have selected all images$")
    public void iHaveSelectedAllImages() {
        Assert.assertEquals(uploadScreen.listUploadedPictures.size(), uploadScreen.selectedImages.size());
    }

    @Then("^I have selected no images$")
    public void iHaveNoImagesSelected() {
        Assert.assertEquals(0, uploadScreen.selectedImages.size());
    }

    @Then("^All images have the same caption \"([^\"]*)\"$")
    public void allImagesHaveTheSameCaption(String caption) throws Throwable {
        Assert.assertTrue(uploadScreen.listUploadedPictures.size() > 0);
        uploadScreen.listUploadedPictures.forEach(webElement -> {
            Assert.assertEquals(caption, getCaptionValue(webElement));
        });
    }


    @And("^I can specify a caption for the selected images\"([^\"]*)\"$")
    public void iCanSpecifyACaptionForTheSelectedImages(String caption) {
        uploadScreen.fillInMetaDataForOneImage(caption, "picture squad", caption);
    }

    @And("^I upload \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iUploadAndAnd(String firstFilePath, String secondFilePath, String thirdFilePath) throws Throwable {
        uploadScreen.uploadFiles(new String[]{firstFilePath, secondFilePath, thirdFilePath});
    }

    @And("^I select the first \"([^\"]*)\" images$")
    public void iSelectTheFirstImages(String amount) {
        uploadScreen.selectImages(Integer.parseInt(amount));
    }

    @Then("^Only the selected images have the same caption \"([^\"]*)\"$")
    public void onlyTheSelectedImagesHaveTheSameCaption(String caption) throws Throwable {
        uploadScreen.notSelectedImages.stream()
            .map(this::getCaptionValue)
            .forEach(captionValue -> {
                Assert.assertNotEquals(caption, captionValue);
            });
        uploadScreen.selectedImages.stream()
                .map(this::getCaptionValue)
                .forEach(captionValue -> Assert.assertEquals(caption, String.valueOf(captionValue)));
    }

    public String getCaptionValue(WebElement webElement) {
        return webElement.findElement(By.cssSelector("span.description__caption--full")).getText();
    }

    @Then("^I have \"([^\"]*)\" images selected$")
    public void iHaveImagesSelected(String amount) {
        Assert.assertEquals(Integer.parseInt(amount), uploadScreen.selectedImages.size());
    }

    @Then("^The metadata sidebar is available$")
    public void theMetadataSidebarIsAvailable() {
        Assert.assertTrue("the metadata fields are not available",driver.findElement(By.xpath("//input[contains(@name, 'caption')]")).isDisplayed());
    }

    @And("^the number of selected images is \"([^\"]*)\"$")
    public void theNumberOfSelectedImagesIs(String numberOfImages) {
        Assert.assertEquals("number of selected images is not correct.",numberOfImages,uploadScreen.numberOfSelectedImages.getText());
    }

    @And("^I remove brand \"([^\"]*)\" for image number \"([^\"]*)\"$")
    public void iRemoveBrandForImageNumber(String brand, int imageNr){
        Assert.assertTrue("Could not remove brand "+brand+" for image number "+imageNr,uploadScreen.removeBrandForImage(brand, imageNr));
    }

    @Then("^the brands \"([^\"]*)\" are selected for image number \"([^\"]*)\"$")
    public void theBrandsAreSelectedForImageNumber(List<String> brands, int imageNumber){
       Assert.assertTrue("one or more of the following brands are not selected: "+brands,uploadScreen.areBrandsSelectedForImage(brands,imageNumber));
    }

    @Then("^I get \"([^\"]*)\" image in error$")
    public void iGetImageInError(int numberOfImages)  {
        Assert.assertTrue("number of images in error is not "+numberOfImages,uploadScreen.isNumberOfErrorsCorrect(numberOfImages));
    }

    @And("^I can only select brands \"([^\"]*)\"$")
    public void iCanOnlySelectBrands(List<String> expectedBrands) {
        Assert.assertTrue("Brand choice is not restriced",uploadScreen.isBrandChoiceRestricted(expectedBrands));
    }
}
