package steps;

import com.google.common.base.Strings;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.utils.URIBuilder;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import screens.OneLoginScreen;
import screens.ResultScreen;
import screens.SearchScreen;
import screens.UploadScreen;
import webdriver.WebDriverManagerUtil;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

public class search {

    //Run chrome headless in the pipeline
    WebDriver driver;
    //WebDriver driver=WebDriverManagerUtil.setupDriver("chrome");
    OneLoginScreen oneLoginScreen;
    ResultScreen resultScreen;
    UploadScreen uploadScreen;
    SearchScreen searchScreen;
    List<WebElement> resultList;
    private String searchPageUrlForBrand = WebDriverManagerUtil.getLink() + "search?brand=";

    private static final int DEFAULT_PAGE_SIZE = 60;
    private static final Log LOGGER = LogFactory.getLog(search.class);

    public void setup(Scenario scenario) {
        driver = WebDriverManagerUtil.setupDriver(scenario);
        oneLoginScreen = new OneLoginScreen(driver);
        resultScreen = new ResultScreen(driver);
        uploadScreen = new UploadScreen(driver);
        //Runtime.getRuntime().addShutdownHook(new Thread(() -> driver.close()));
    }

    @Before
    public void login(Scenario scenario) {
        setup(scenario);
        oneLoginScreen.login("srvphotohub@persgroep.net","K9crMGEcnv#Q");
    }

    @Given("^I am on the home page$")
    public void iAmOnTheHomePage() {
        LOGGER.info("Opening home page");
        searchScreen=new SearchScreen(driver);
        resultScreen.waitForList(WebDriverManagerUtil.getLink());
    }

    @Given("^I am on the home page for brand \"([^\"]*)\"$")
    public void iAmOnTheHomePageForBrand(String brand) throws URISyntaxException {
        navigateToSearchPage(Optional.of(brand),Optional.empty(),true);
    }

    @Given("^I am on the home page for brand \"([^\"]*)\" and redirect \"([^\"]*)\"$")
    public void iAmOnTheHomePageForBrandAndRedirect(String brand, String redirect) throws URISyntaxException {
        navigateToSearchPage(Optional.of(brand),Optional.of(redirect),true);
    }

    @Given("^I am on the zoomed-in home page$")
    public void iAmOnTheZoomedInHomePage() {
        LOGGER.info("Opening zoomed-in home page");
        searchScreen=new SearchScreen(driver);
        resultScreen.waitForList(WebDriverManagerUtil.getLink());
        searchScreen.zoomIn(110);
        searchScreen.searchButton.isDisplayed();
    }

    @When("^I change the UUID to collaborator \"([^\"]*)\"$")
    public void iChangeTheUUIDToCollaborator(String collaboratorAlias) throws URISyntaxException {
        navigateToAlias(collaboratorAlias);
    }

    private void navigateToAlias(String userAlias) throws URISyntaxException {
        searchScreen=new SearchScreen(driver);
        URIBuilder uriBuilder = new URIBuilder(WebDriverManagerUtil.getLink()+"alias");
        uriBuilder.setParameter("useralias",userAlias);
        String url = uriBuilder.build().toString();
        LOGGER.info("Navigating to: '" + url + "'");
        driver.get(url);
    }

    private void navigateToSearchPage(Optional<String> brand, Optional<String> redirect, boolean expectResults) throws URISyntaxException {

        LOGGER.info("Opening home page for brand " + brand + " and redirect" + redirect);
        searchScreen=new SearchScreen(driver);
        String url = buildURLWithParameters(brand, redirect);
        LOGGER.info("Navigating to: '" + url + "'");
        driver.get(url);
        if(expectResults)
            resultScreen.waitForList(url);
    }

    private void navigateToUploadPage() throws URISyntaxException {
        LOGGER.info("Opening search page.");
        searchScreen=new SearchScreen(driver);
        String url = WebDriverManagerUtil.getLink()+"upload";
        LOGGER.info("Navigating to: '" + url + "'");
        driver.get(url);
    }

    private String buildURLWithParameters(Optional<String> brand, Optional<String> redirect) throws URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder(WebDriverManagerUtil.getLink()+"search");
        if(brand.isPresent())
            uriBuilder.setParameter("brand",brand.get());
        if(redirect.isPresent())
            uriBuilder.setParameter("redirect",redirect.get());
        return uriBuilder.build().toString();
    }

    @When("^I search for \"([^\"]*)\"$")
    public void iSearchFor(String searchTerm) {
        searchScreen.typeInclude(Strings.nullToEmpty(searchTerm));
    }

    @When("^I scroll down the result page$")
    public void iScrollDownTheResultPage() { resultScreen.scrollAfterSearch(); }

    @When("^I scroll down without search$")
    public void iScrollDownWithoutSearch() { resultScreen.scroll(); }

    @And("^I scroll down to the last page$")
    public void iScrollDownToTheLastPage() {
        System.out.println("number of results "+resultScreen.getNumberOfResults());
        System.out.println("number of scroll before reaching the last page "+resultScreen.getNumberOfResults()/60);
        for(int i=0;i<resultScreen.getNumberOfResults()/60;i++) {
            System.out.println("scroll number "+ (i+1));
            resultScreen.scrollAfterSearch();
        }
    }

    @When("^I search for \"([^\"]*)\" and \"([^\"]*)\"$")
    public void iSearchForAnd(String includedTerm, String excludedTerm){
        LOGGER.info("Starting search.");

        String include = Strings.nullToEmpty(includedTerm);
        String exclude = Strings.nullToEmpty(excludedTerm);

        LOGGER.info("Including search term: " + include);
        LOGGER.info("Excluding search term: " + exclude);

        searchScreen.typeInclude(include);
        searchScreen.typeExclude(exclude);
    }

    @And("^I press search$")
    public void iPressSearch(){
        searchScreen.clickSearch();
    }

    @Then("^I expect to get \"([^\"]*)\"$")
    public void iExpectToGet(String result) {
        int resultSize = getResult().size();
        boolean hasResults = result.equalsIgnoreCase("results") || result.equalsIgnoreCase("result");

        LOGGER.info("Found a list of " + resultSize + " results.");

        if(hasResults) {
            Assert.assertTrue("Expected a result but got an empty result list",resultSize!=0);
            if(resultSize==0)
                resultScreen.takeScreenshot();
        } else {
            Assert.assertEquals("Expected no results but got a list of " + resultSize + " results", 0, resultSize);
        }
    }

    @And("^image is visible when I search for upload's caption$")
    public void imageIsVisibleWhenISearchForUploadSCaption() {
        searchScreen.typeInclude(uploadScreen.getCaptionToSearch());
        searchScreen.clickSearch();
        getResult();
        Assert.assertTrue("Expected a result but got an empty result list",getResult().size()>=1);
        if(getResult().size()==0)
            searchScreen.takeScreenshot();
    }

    @Then("^I expect to get the next page of images$")
    public void iExpectNextPageOfImages() {
        int resultSize = getResult().size();
        LOGGER.info("found a list of "+resultSize+" results.");
        Assert.assertEquals("scroll didn't work. Number of elements on the page is not the one expected.",DEFAULT_PAGE_SIZE * 2, resultSize);
    }

    @After
    public void closeBrowser(){
        WebDriverManagerUtil.getInstance().removeDriver();
    }

    public List<WebElement> getResult() {
        return resultScreen.getResultList();
    }

    @Then("^some images are alerted$")
    public void someImagesAreAlerted() {
        List<WebElement> articles = driver.findElements(By.tagName("article"));
        boolean allAlerted = articles.stream()
                .anyMatch(webElement -> webElement.getAttribute("class").contains("search-result--warning"));

        Assert.assertEquals(DEFAULT_PAGE_SIZE * 2, articles.size());
        Assert.assertTrue("Not all images are alerted!", allAlerted);
    }

    @And("^Search fields are present \"([^\"]*)\" and \"([^\"]*)\"$")
    public void searchFieldsArePresentAnd(String include, String exclude) {
        Assert.assertEquals(include, searchScreen.includingField.getAttribute("value"));
        Assert.assertEquals(exclude, searchScreen.excludingField.getAttribute("value"));
    }

    @When("^I change the view option to small images$")
    public void iChangeTheViewOptionToSmallImages() {
        Assert.assertTrue("Image view size should be large", searchScreen.checkImageViewSizeLargeIsSelected());
        searchScreen.setImageViewSizeToSmall();
    }

    @Then("^I expect to get the result displayed with small images$")
    public void iExpectToGetTheResultDisplayedWithSmallImages() {
        Assert.assertTrue("result grid does not contains small images", resultScreen.isContainingSmallImages());
    }

    @Then("^The loading is not visible on the last page$")
    public void theLoadingIsNotVisibleOnTheLastPage() {
        resultScreen.scrollAfterSearch();
        Assert.assertFalse("Loader is displayed at the end of the results", resultScreen.isLoaderPresent());
    }

    @And("^I remove the shooting date using the cross$")
    public void iRemoveTheShootingDateUsingTheCross() {
        searchScreen.removeShootingDate();
    }

    @Then("^I expect the \"([^\"]*)\" parameter to be empty$")
    public void iExpectTheParameterToBeEmpty(String parameter) throws MalformedURLException {
        Assert.assertEquals("the parameter "+parameter+" is not empty", "",searchScreen.getParameter(parameter));
    }

    @And("^I navigate to the search page$")
    public void iNavigateToTheSearchPage() throws URISyntaxException {
        navigateToSearchPage(Optional.empty(),Optional.empty(),false);
    }

    @And("^I navigate to the upload page$")
    public void iNavigateToTheUploadPage() throws URISyntaxException {
        navigateToUploadPage();
    }
}
