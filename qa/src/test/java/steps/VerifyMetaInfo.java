package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import net.persgroep.diocontent.images.api.asset.Asset;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import screens.FullViewScreen;
import screens.ResultScreen;
import utils.MetadataUtils;
import webdriver.WebDriverManagerUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import static utils.MetadataUtils.*;

public class VerifyMetaInfo {
    private static final Log logger = LogFactory.getLog(VerifyMetaInfo.class);
    WebDriver driver = WebDriverManagerUtil.getDriver();
    ResultScreen resultScreen=new ResultScreen(driver);
    FullViewScreen fullViewScreen = new FullViewScreen(driver);
    List<WebElement> resultList=resultScreen.getResultList();
    String imageId;

    @Then("^I see the meta information on hovering$")
    public void iSeeTheMetaInformationOnHovering() {
        for (WebElement result : resultList) {

            resultScreen.clickOnMetaArrow(result);
            imageId = resultScreen.getMetaInfoValue(result, "ID");
            logger.info("Getting metadata of image with ID: " + imageId);
            Asset asset = MetadataUtils.getAsset(Integer.parseInt(imageId));

            String caption = result.findElements(By.className("search-result__description")).get(1).getText().replaceAll("\\s+", "").replaceAll("\\u00a0", "");

            Assert.assertTrue("meta info is not displayed", result.findElement(By.className("search-result__meta")).isDisplayed());
            Assert.assertEquals("Image with id " + imageId + " has wrong Caption", getCaption(asset), caption);
            Assert.assertEquals("Image with id " + imageId + " has wrong Credits value", getCredits(asset), resultScreen.getMetaInfoValue(result, "Credits"));
            Assert.assertEquals("Image with id " + imageId + " has wrong photographer", getPhotographer(asset), resultScreen.getMetaInfoValue(result, "Photographer"));
            Assert.assertEquals("Image with id " + imageId + " has wrong import date", getCreateDate(asset), resultScreen.getMetaInfoValue(result, "Import date"));
            Assert.assertEquals("Image with id " + imageId + " has wrong comes from", getComesFrom(asset), resultScreen.getMetaInfoValue(result, "Comes from"));
            Assert.assertEquals("Image with id " + imageId + " has wrong Shooting date", getShootingDate(asset), resultScreen.getMetaInfoValue(result, "Shooting date"));
            Assert.assertEquals("Image with id " + imageId + " has wrong Location", getLocation(asset), resultScreen.getMetaInfoValue(result, "Location"));

            resultScreen.clickOnMetaArrow(result);
        }
    }

    @And("^The price matches the price of diocontent$")
    public void thePriceMatchesThePriceOfDiocontent() {
        resultScreen.getResultList()
                .forEach(this::comparePriceInfo);
    }

    private void comparePriceInfo(WebElement webElement) {
        MetadataUtils.resetAssetData();
        resultScreen.openFullView(webElement);
        String id = fullViewScreen.getMetaInfoValue(driver.findElement(By.className("preview")), "ID");
        logger.info("Sending request to diocontent for ID: " + id);
        Asset asset = MetadataUtils.getAsset(Integer.parseInt(id));
        Price price = Price.getPrice(asset.getPriceRange().getValue());
        logger.info("Found price "+ price +" for image id " + id);
        resultScreen.closeFullView();
        if (price != Price.NOTSET) {
            Assert.assertTrue(webElement.findElement(By.cssSelector("span.search-result__price-range.search-result__" + price.getClassName())).isDisplayed());
            comparePriceFullView(webElement, price);
        }
    }

    private void comparePriceFullView(WebElement webElement, Price price) {
        logger.info("Comparing the price in the Full image view");
        resultScreen.openFullView(webElement);
        waitForFullImageView();
        Assert.assertEquals("price is not the expected one",price.getValue().toLowerCase(),fullViewScreen.priceRange.getText().toLowerCase());
        resultScreen.closeFullView();
    }

    private void waitForFullImageView() {
        logger.info("Waiting for full image view to come up...");
        new WebDriverWait(driver, 60).until((ExpectedCondition<Boolean>) webDriver ->
                webDriver.findElement(By.cssSelector("div.preview")).isDisplayed()
        );
    }
}
