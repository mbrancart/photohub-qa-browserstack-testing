package utils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.iptc.IptcDirectory;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import webdriver.WebDriverManagerUtil;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static com.drew.metadata.iptc.IptcDirectory.TAG_CAPTION;
import static com.drew.metadata.iptc.IptcDirectory.TAG_CREDIT;
import static com.google.common.util.concurrent.Uninterruptibles.sleepUninterruptibly;
import static java.util.Arrays.stream;
import static java.util.concurrent.TimeUnit.SECONDS;

public class DownloadOriginal {

    private String INPUT_FOLDER = WebDriverManagerUtil.downloadFolder;
    private String OUTPUT_FOLDER = INPUT_FOLDER + "/files";
    private WebDriver driver = WebDriverManagerUtil.getDriver();

    public utils.Metadata readSingleImageMetadata() {
        return readSingleImageMetadata(this.getSingleFilePresentInInputFolder());
    }

    private File getSingleFilePresentInInputFolder() {
        File[] listOfFiles = getAllFilesInInputFolder();
        System.out.println("inputfolder :"+INPUT_FOLDER);
        System.out.println("number of elements presents in the folder : "+listOfFiles.length);
        printListOfFiles(listOfFiles);

        return listOfFiles[0];
    }

    private File[] getAllFilesInInputFolder() {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        System.out.println("File exists ??? " + jse.executeScript("browserstack_executor: {\"action\": \"fileExists\", \"arguments\": {\"fileName\": \"C:/Users/hello/Downloads\"}}"));

        String base64EncodedFile = (String)jse.executeScript("browserstack_executor: {\"action\": \"getFileContent\", \"arguments\": {\"fileName\": \"C:\\Users\\hello\\Downloads\"}}");
        byte[] data = Base64.getDecoder().decode(base64EncodedFile);
        OutputStream stream = null;
        try {
            stream = new FileOutputStream("karma-browserstack-launcher-0.1.5.zip");
            stream.write(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        File folder = new File("Downloads");
        return folder.listFiles();
    }

    private void printListOfFiles(File[] listOfFiles) {
        stream(listOfFiles).forEach(f -> System.out.println("filename = "+ f.getPath()));
    }

    public utils.Metadata readSingleImageMetadata(File file) {
        try{
            return readMetadataFromFile(file);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("failed reading metadata ", e);
        }
    }

    private utils.Metadata readMetadataFromFile(File file) throws ImageProcessingException, IOException {
        Directory directory = readDirectory(file);
        return createMetadataFromDirectory(directory);
    }

    private Directory readDirectory(File file) throws ImageProcessingException, IOException {
        Metadata metadata = ImageMetadataReader.readMetadata(file);
        return metadata.getFirstDirectoryOfType(IptcDirectory.class);
    }

    private utils.Metadata createMetadataFromDirectory(Directory directory) {
        return new utils.Metadata(directory.getString(TAG_CAPTION),
                directory.getString(TAG_CREDIT));
    }

    public List<utils.Metadata> readZipMetadata(){
        sleepUninterruptibly(20, SECONDS);
        File zipFile = getSingleFilePresentInInputFolder();
        return readZipMetadata(zipFile);
    }

    private List<utils.Metadata> readZipMetadata(File zipFile) {
        List<utils.Metadata> listMetadata = new ArrayList<>();
        unzipFileToOutputFolder(zipFile);
        stream(getAllFilesInOutputFolder()).forEach(f -> listMetadata.add(readSingleImageMetadata(f)));

        return listMetadata;
    }

    private void unzipFileToOutputFolder(File zipFile) {
        unzipArchive(zipFile,new File(OUTPUT_FOLDER));
    }

    private File[] getAllFilesInOutputFolder(){
        File folder = new File(OUTPUT_FOLDER);
        return folder.listFiles();
    }

    public void unzipArchive(File zipFile, File destDir) {
        try {
            tryUnzipArchive(zipFile,destDir);
        }
        catch (Exception ex){
            System.out.println("something went wrong while unzipping: " + ex.getMessage());
        }
    }

    public void tryUnzipArchive(File zipFile, File destDir) throws IOException {
        String destDirectory = destDir.getAbsolutePath();
        try (ArchiveInputStream zipArchiveInputStream = createZipArchiveInputStream(zipFile)) {
            ArchiveEntry processedEntry;
            do {
                processedEntry = processNextEntryInArchive(destDirectory, zipArchiveInputStream);
            } while (processedEntry != null);
        }
    }

    private ZipArchiveInputStream createZipArchiveInputStream(File zipFile) throws FileNotFoundException {
        return new ZipArchiveInputStream(new
                FileInputStream(zipFile), "UTF-8", false, true);
    }

    private ArchiveEntry processNextEntryInArchive(String destDirectory, ArchiveInputStream zipArchiveInputStream) throws IOException {
        ArchiveEntry entry = zipArchiveInputStream.getNextEntry();
        if (entry != null) {
            processEntry(destDirectory, zipArchiveInputStream, entry);
        }
        return entry;
    }

    private void processEntry(String destDirectory, ArchiveInputStream i, ArchiveEntry entry) throws IOException {
        String name = destDirectory + File.separator + entry.getName();
        File f = new File(name);
        File parent = f.getParentFile();
        parent.mkdirs();
        try (OutputStream o = Files.newOutputStream(f.toPath())) {
            IOUtils.copy(i, o);
        }
    }

}
