package utils;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.time.LocalDate;
import java.util.TimeZone;

@Ignore
public class BrusselsDateTimeToSystemTimeZoneConverterTest {

    @Test
    public void convertToSystemTimeZone_whenSystemTimeZoneIsLondonTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/London"));
        String brusselsDateTime = "05/07/201900:09";
        final LocalDate convertedDateTime = BrusselsDateTimeToSystemTimeZoneConverter.convert(brusselsDateTime);
        Assert.assertEquals("2019-07-04", convertedDateTime.toString());
    }

    @Test
    public void convertToSystemTimeZone_whenSystemTimeZoneIsBrusselsTimeZone() {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Brussels"));
        String brusselsDateTime = "05/07/201900:09";
        final LocalDate convertedDateTime = BrusselsDateTimeToSystemTimeZoneConverter.convert(brusselsDateTime);
        Assert.assertEquals("2019-07-05", convertedDateTime.toString());
    }
}