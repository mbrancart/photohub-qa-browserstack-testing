package utils;

import java.util.Objects;

public class Metadata {
    private String caption;
    private String credit;

    public Metadata(String caption, String credit) {
        this.caption = caption;
        this.credit = credit;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Metadata metadata = (Metadata) o;
        if(!caption.equals(metadata.caption))
            System.out.printf("Caption %s is not the same as %s\n",caption,metadata.caption);
        if(!credit.equals(metadata.credit))
            System.out.printf("Credit %s is not the same as %s\n",credit,metadata.credit);
        return Objects.equals(caption, metadata.caption) &&
                Objects.equals(credit, metadata.credit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(caption, credit);
    }
}
