package utils;

import com.google.common.base.Strings;
import net.persgroep.diocontent.images.api.asset.Asset;
import net.persgroep.diocontent.images.api.asset.ImageInfo;
import net.persgroep.diocontent.images.api.asset.MediaPool;
import net.persgroep.diocontent.images.api.client.DioContentClient;
import net.persgroep.diocontent.images.client.DefaultRestDioContentClient;
import org.apache.commons.lang.time.DateUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MetadataUtils {

    private static DioContentClient dioContent
            = new DefaultRestDioContentClient("https://diocontentservice.test.picture.persgroep.cloud/rest", "_API_TEST_ReadWriteDelete", "DioContent");
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private static Asset asset=null;
    private static ImageInfo imageInfo = null;
    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static Asset getAsset(int assetId) {
        return dioContent.getAsset(assetId);
    }

    public static void deleteAsset(int assetId){ dioContent.deleteAsset(assetId); }

    public static void resetAssetData() {
        asset=null;
        imageInfo = null;
    }

    public static List<String> getMediapoolNames(int assetId) {
        return dioContent.getAsset(assetId).getMediaPools().stream()
                .map(MediaPool::getName)
                .collect(Collectors.toList());
    }

    public static String getCredits(Asset asset) {
        return replaceCharacters(asset.getAdditionalInfo().getCredit());
    }

    public static String getCaption(Asset asset) {
        return replaceCharacters(asset.getAdditionalInfo().getCaption());
    }

    public static String getPhotographer(Asset asset) {
        return replaceCharacters(asset.getAdditionalInfo().getPhotographer());
    }

    public static String getShootingDate(Asset asset) {
        return asset.getShootDate() != null ? dateFormat.format(asset.getShootDate().toDate()) : "";
    }

    public static String replaceCharacters(final String input) {
        return Strings.nullToEmpty(input).replaceAll("\\s+","")
                .replaceAll("\\u00a0","");
    }

    public static String getCreateDate(Asset asset) {
        return dateFormat.format(asset.getCreateDate());
    }

    public static String getComesFrom(Asset asset) {
        return replaceCharacters(asset.getComesFrom());
    }

    public static String getLocation(Asset asset) {
        StringBuilder builder = new StringBuilder();

        Optional<String> city = Optional.ofNullable(asset.getAdditionalInfo().getCity());
        Optional<String> country = Optional.ofNullable(asset.getAdditionalInfo().getCountry());

        builder.append(city.orElse(""));
        country.ifPresent(value -> builder.append(city.isPresent() ? ", " + value : value));
        return replaceCharacters(builder.toString());
    }

}
