package utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public class BrusselsDateTimeToSystemTimeZoneConverter {

    public static LocalDate convert(String brusselsDateTimeText) {
        return parseDateTimeAsBrusselsTimeZoneToSystemTimeZone(brusselsDateTimeText);
    }

    private static LocalDate parseDateTimeAsBrusselsTimeZoneToSystemTimeZone(String dateTimeText) {
        LocalDateTime localDateTime = parseToLocalDateTime(dateTimeText);
        return convertLocalDateTimeFromTimeZoneBrusselsToZimeZoneOfTheSystem(localDateTime);
    }

    private static LocalDate convertLocalDateTimeFromTimeZoneBrusselsToZimeZoneOfTheSystem(LocalDateTime localDateTime) {
        ZonedDateTime brusselsDateTime = putLocalDateTimeInTimeZoneOfBrussels(localDateTime);
        return getSameDateTimeAsInSystemTimeZone(brusselsDateTime).toLocalDate();
    }

    private static LocalDateTime parseToLocalDateTime(String dateTimeText) {
        return LocalDateTime.parse(dateTimeText, DateTimeFormatter.ofPattern("dd/MM/yyyyHH:mm"));
    }

    private static ZonedDateTime putLocalDateTimeInTimeZoneOfBrussels(LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.of("Europe/Brussels"));
    }

    private static ZonedDateTime getSameDateTimeAsInSystemTimeZone(ZonedDateTime zonedDateTime) {
        return zonedDateTime.withZoneSameInstant(getSystemTimeZoneId());
    }

    private static ZoneId getSystemTimeZoneId() {
        return TimeZone.getDefault().toZoneId();
    }
}
