package datefilters;

import java.time.LocalDate;

public interface Timespan {
    LocalDate from();
    LocalDate to();
}
