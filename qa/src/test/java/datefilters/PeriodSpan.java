package datefilters;

import java.time.LocalDate;
import java.util.Arrays;

// should be replaced by class in Photohub project
public enum PeriodSpan implements Timespan{
    ALL {
        public LocalDate from() { return null; }
        public LocalDate to()   { return LocalDate.now(); }
    }, TODAY {
        public LocalDate from() { return LocalDate.now(); }
        public LocalDate to()   { return LocalDate.now(); }
    }, YESTERDAY {
        public LocalDate from() { return LocalDate.now().minusDays(1); }
        public LocalDate to()   { return LocalDate.now().minusDays(1); }
    }, TODAY_AND_YESTERDAY {
        public LocalDate from() { return LocalDate.now().minusDays(1); }
        public LocalDate to()   { return LocalDate.now(); }
    }, PAST_THIRTY_DAYS {
        public LocalDate from() { return LocalDate.now().minusMonths(1); }
        public LocalDate to()   { return LocalDate.now(); }
    };

    public static PeriodSpan toTimeSpanOrDefault(String span, PeriodSpan defaultSpan) {
        return Arrays.stream(values())
                .filter(timespan -> timespan.toString().equalsIgnoreCase(span))
                .findFirst()
                .orElse(defaultSpan);
    }
}
