package datefilters;

import java.util.Arrays;

public enum Filter {
    SHOOTINGDATE("Shooting date"),
    IMPORTDATE("Import date");

    private final String filterName;

    Filter(String filterName) {
        this.filterName = filterName;
    }

    public static Filter getFilter(String filterName) {
        return Arrays.stream(Filter.values())
                .filter(filter -> filter.filterName.equalsIgnoreCase(filterName))
                .findAny()
                .orElseThrow(FilterNotFoundException::new);
    }

    public String getFilterName() {
        return this.filterName;
    }

    private static class FilterNotFoundException extends RuntimeException {
        public FilterNotFoundException() {
            super("No match found for the given filterName");
        }
    }
}
