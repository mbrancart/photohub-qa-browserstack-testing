
Feature: verifyPagination

  Scenario Outline: Selecting the amount of images per page should the right amount
    Given I am on the home page
    When I select the page size amount "<Amount>"
    Then I expect to get "<Amount>" results
    Examples:
      | Amount  |
      | 20      |
      | 40      |
      | 60      |

  Scenario: In traditional pagination a click on the button should trigger new results
    Given I am on the home page
    When I select the page size amount "20"
    Then I expect to get "results"
    Then I press the nextPage button
    Then I expect to get a new page of images

  Scenario: In traditional pagination on the first page there should be no previous page button
    Given I am on the home page
    When I select the page size amount "20"
    Then I expect to get "results"
    Then I go to the first page
    Then I expect to get "results"
    Then I expect there is no previous page button

  Scenario: In traditional pagination on the last page there should be no next page button
    Given I am on the home page
    When I search for "test"
    And I press search
    And I select the page size amount "20"
    Then I expect to get "results"
    Then I go to the last page
    Then I expect to get "results"
    Then I expect there is no next page button

  Scenario: Verify that next page is displayed on result page
    Given I am on the home page
    When I select the page size amount "20"
    When I open the full view of an image "18"
    And  I select the next "5" images
    And I close the full view
    Then I expect to get a new page of images

  Scenario: Verify that previous page is displayed on result page
    Given I am on the home page
    When I select the page size amount "20"
    And I go to page number "10"
    When I open the full view of an image "1"
    And  I select the previous "5" images
    And I close the full view
    Then I expect to get the previous page of images

  Scenario Outline: Verify that I see the first page when toggling pagination styles - traditional - traditional
    Given I am on the home page
    When I select the page size amount "<Before>"
    And I go to page number "10"
    And I select the page size amount "<After>"
    Then I expect to get "results"
    Then I expect to get the first page
    Examples:
      | Before  | After |
      | 20      | 40    |
      | 40      | 20    |

#  Scenario Outline: Verify that I see the first page when toggling pagination styles - infinite - traditional
#    Given I am on the home page
#    When I select the page size amount "60"
#    And I scroll down without search
#    And I select the page size amount "<After>"
#    Then I expect to get "results"
#    Then I expect to get the first page
#    Examples:
#      | After |
#      | 40    |
#      | 20    |

  Scenario Outline: Verify that I see the first page when toggling pagination styles - traditional - infinite
    Given I am on the home page
    When I select the page size amount "<Before>"
    And I go to page number "10"
    And I select the page size amount "60"
    Then I expect to get "results"
    Then I expect to get "60" results
    Examples:
      | Before  |
      | 40      |
      | 20      |

