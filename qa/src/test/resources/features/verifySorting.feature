
Feature: Verify sorting of results

  Scenario: Verify the default sorting without search
    Given I am on the home page
    Then Results are ordered by "Most recent (import date)"

  Scenario Outline: Verify sorting functionality after first change
    Given I am on the home page
    When I search for "<searchIncluded>"
    And I press search
    And I order results by "<sorting>"
    Then Results are ordered by "<sorting>"

    Examples:
      | searchIncluded | sorting                     |
      | bla            | Oldest (import date)        |
      |                | Most recent (shooting date) |
      |                | Oldest (shooting date)      |


  Scenario Outline: Verify sorting functionality after multiple changes
    Given I am on the home page
    When I search for "<searchIncluded>" and "<searchExcluded>"
    And I order results by "<firstSortingOrder>"
    And I order results by "<secondSortingOrder>"
    Then Results are ordered by "<secondSortingOrder>"
    And Search fields are present "<searchIncluded>" and "<searchExcluded>"

    Examples:
      | searchIncluded | searchExcluded | firstSortingOrder           | secondSortingOrder          |
      | some           | Word           | Oldest (import date)        | Most recent (import date)   |
      | Obama          |                | Most recent (import date)   | Oldest (import date)        |
      | Obama          |                | Most recent (shooting date) | Oldest (import date)        |
      | Barack         | Obama          | Oldest (shooting date)      | Most recent (import date)   |