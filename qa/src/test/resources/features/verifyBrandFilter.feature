
Feature: verify brand filter

  Scenario Outline: verify context brand selection
    Given I am on the home page for brand "<contextbrand>"
    When I press search
    Then I expect to get "results"
    And Brand filter is still "<brand>"
    Examples:
      | contextbrand                    | brand         |
      | net.persgroep.brand.demorgen    | De Morgen     |
      | net.persgroep.brand.humo        | Humo          |

  Scenario Outline: Verify visible brands for collaborator having access to everything
    Given I am on the home page
    When I press search
    Then I expect to get "results"
    And the brands "<availableBrands>" are available in the dropdown
    And Displayed images are images from "<mediapools>"
    Examples:
      | availableBrands                                                                                                                                                                                                                                                                    | mediapools |
      | All Brands, AD, Brabants Dagblad, Dag Allemaal, De Gelderlander, De Morgen, BN DeStem, De Stentor, Eindhovens Dagblad, Goed Gevoel, Het Laatste Nieuws, Humo, Het Parool, Primo, PZC, Story, Trouw, Tubantia, Teve-blad, TV Familie, de Volkskrant | all        |

  Scenario Outline: Verify visible brands on upload screen
    Given I change the UUID to collaborator "<collaboratorAlias>"
    And I navigate to the upload page
    And I upload "images/cartoon-animation.gif"
    Then My file is visible in the list
    And I select all images
    And I can only select brands "<availableBrands>"
    Examples:
      | collaboratorAlias | availableBrands        |
      | AD                | AD                     |
      | DM_GG             | De Morgen, Goed Gevoel |

