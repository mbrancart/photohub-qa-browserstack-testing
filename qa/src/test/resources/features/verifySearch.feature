
Feature: search

  Scenario Outline: search for simple things
    Given I am on the home page
    When I search for "<included>" and "<excluded>"
    And I press search
    Then I expect to get "<Result>"
    Examples:
      | included | excluded | Result     |
      |          |          | Results    |
      | a        |          | Result     |
      | a        | a        | no Results |

  Scenario Outline: search for including only AND NOT or OR
    Given I am on the home page
    When I search for "<included>"
    And I press search
    Then I expect to get "<Result>"
    Examples:
      | included | Result     |
      | AND      | Results    |
      | NOT      | Results    |
      | OR       | Results    |

  Scenario Outline: search for including invalid terms
    Given I am on the home page
    When I search for "<included>"
    And I press search
    Then I expect to get "<Result>"
    Examples:
      | included | Result     |
      | \*       | no results |
      | !        | no results |

  Scenario: change result view to small images
    Given I am on the home page
    When I change the view option to small images
    Then I expect to get the result displayed with small images


  Scenario Outline: Verify that loading is not visible when I reach the end of the results
    Given I am on the home page
    When I select a shooting date period of "<selectedShootingDate>"
    And I select a import date period of "<selectedImportDate>"
    And I press search
    And I scroll down to the last page
    Then The loading is not visible on the last page
    Examples:
      | selectedImportDate | selectedShootingDate |
      | Today & Yesterday  | All                  |

    Scenario: verify pagination when zoomed in
      Given I am on the zoomed-in home page
      And I scroll down without search
      Then I expect to get the next page of images
