
Feature: Verify editing in upload

  Scenario Outline: Verify if the dynamic form is present when selected
    Given I am on the home page
    When I open the upload tab
    And I upload "<filePath>"
    Then My file is visible in the list
    And Error is displayed when caption and credits are empty
    Examples:
      | filePath            |
      | images/wallpaper4.jpg |

  Scenario Outline: Verify all images are updated when i edit caption while all images are selected
    Given I am on the home page
    When I open the upload tab
    And I upload "<filePath>" and "<otherFilePath>"
    And My file is visible in the list
    And I select all images
    And I can specify a caption for the selected images"<caption>"
    Then All images have the same caption "<caption>"
    Examples:
      | filePath              | otherFilePath     | caption   |
      | images/wallpaper4.jpg | images/icon2.jpeg | myCaption |


  Scenario Outline: Verify caption only updated to selected images
    Given I am on the home page
    When I open the upload tab
    And I upload "<filePath>" and "<otherFilePath>" and "<thirdFilePath>"
    And My file is visible in the list
    And I select the first "<amount>" images
    And I can specify a caption for the selected images"<caption>"
    Then Only the selected images have the same caption "<caption>"
    Examples:
      | filePath          | otherFilePath         | thirdFilePath   | amount | caption   |
      | images/icon2.jpeg | images/wallpaper4.jpg | images/icon.png | 1      | myCaption |



