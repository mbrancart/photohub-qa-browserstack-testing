# README #

This project is used to run the automated QA tests against the photohub application.

### Running tests via maven
You can run all the tests via maven as follows:

````
mvn test
````

which is equal to 
````
mvn test -Pdev
````

This will run the all the tests on photohub deployed in the development environment. 

It's possible to run the test suite against photohub in other environments via maven profiles. 

To run the test suite on a locally deployed photohub you can use the command:
````
mvn test -Plocal
````

Other profiles you can use are `nonprod` and `prod` for testing against the nonprod and prod environments.

### Running tests via the IDE

Running the tests from your IDE is also possible, as default they will run againt the development environment.

There are 2 ways you can change this behavior:


1. You can define a system property `photohub.url` which contains the url to the photohub application you want to test. This is also how the maven build is setup to enforce the url for photohub.

2. You can change the code in `webdriver.WebDriverManagerUtil` to return the url to the photohub application you want to test. This will only work if the system property is not defined.